const countsNeeded = 1;
let counts = 1;
let isAnimating = false;

const prizes = [
  {
    image: '3_BRO.png',
    title: '3 BRO'
  },
  {
    image: '1_BRO.png',
    title: '1 BRO'
  },
  {
    image: 'vortis.png',
    title: 'vortis'
  },
  {
    image: 'alhambra.png',
    title: 'alhambra'
  },
  {
    image: 'athenia.png',
    title: 'athenia'
  },
];

const present = document.querySelector('.present');
const giftImage = document.querySelector('.gift');

present.addEventListener('click', () => {
  if (!present.classList.contains('open') && !isAnimating) {
    counts += 1;
    present.style.setProperty('--count', Math.ceil(counts / 2));
    present.classList.add('animate');
    present.classList.add('animate2');
    isAnimating = true;

    setTimeout(() => {
      present.classList.remove('animate');
    }, 300);

    setTimeout(() => {
      present.classList.remove('animate2');
      isAnimating = false;
    }, 3000);

    setTimeout(() => {
      if (counts >= countsNeeded) {
        // Select a random prize
        const randomPrize = prizes[Math.floor(Math.random() * prizes.length)];
        // Update the gift image source and alt text
        giftImage.src = randomPrize.image;
        giftImage.alt = randomPrize.title;
        
        present.classList.add('open');
      }
    }, 3000);
  }
});

const canvas = document.querySelector('canvas');
const ctx = canvas.getContext('2d');

let width;
let height;
let lastNow;
let snowflakes;
let maxSnowflakes = 50; // Reduced max number of stars

const rand = (min, max) => min + Math.random() * (max - min);

class Snowflake {
  constructor() {
    this.spawn(true);
  }

  spawn(anyY = false) {
    this.x = rand(0, width);
    this.y = anyY === true
      ? rand(-50, height + 50)
      : rand(-50, -10);
    this.xVel = rand(-0.05, 0.05);
    this.yVel = rand(0.02, 0.1);
    this.angle = rand(0, Math.PI * 2);
    this.angleVel = rand(-0.001, 0.001);
    this.size = rand(2, 7); // Smaller size
    this.sizeOsc = rand(0.01, 0.5);
  }

  update(elapsed) {
    const xForce = rand(-0.001, 0.001);

    if (Math.abs(this.xVel + xForce) < 0.075) {
      this.xVel += xForce;
    }

    this.x += this.xVel * elapsed;
    this.y += this.yVel * elapsed;
    this.angle += this.xVel * 0.05 * elapsed;

    if (
      this.y - this.size > height
      || this.x + this.size < 0
      || this.x - this.size > width
    ) {
      this.spawn();
    }

    this.render();
  }

  render() {
    ctx.save();
    const { x, y, size, angle } = this;
    ctx.translate(x, y);
    ctx.rotate(angle);

    // Draw a star outline
    ctx.beginPath();
    for (let i = 0; i < 5; i++) {
      ctx.lineTo(0, size);
      ctx.translate(0, size);
      ctx.rotate((Math.PI * 2 / 10));
      ctx.lineTo(0, -size);
      ctx.translate(0, -size);
      ctx.rotate(-(Math.PI * 6 / 10));
    }
    ctx.closePath();
    ctx.strokeStyle = 'yellow'; // Yellow color
    ctx.shadowColor = 'yellow'; // Yellow shadow
    ctx.shadowBlur = 10;
    ctx.lineWidth = 2;
    ctx.stroke();
    ctx.restore();
  }
}

function render(now) {
  requestAnimationFrame(render);

  const elapsed = now - lastNow;
  lastNow = now;

  ctx.clearRect(0, 0, width, height);
  if (snowflakes.length < maxSnowflakes) {
    snowflakes.push(new Snowflake());
  }

  ctx.strokeStyle = 'rgba(255, 255, 255, .5)';
  ctx.fillStyle = 'rgba(255, 255, 255, .5)';

  snowflakes.forEach((snowflake) => snowflake.update(elapsed, now));
}

function resize() {
  width = window.innerWidth;
  height = window.innerHeight;

  canvas.width = width;
  canvas.height = height;

  maxSnowflakes = Math.max(width / 20, 50); // Reduced number of stars based on width
}

function pause() {
  cancelAnimationFrame(render);
}
function resume() {
  lastNow = performance.now();
  requestAnimationFrame(render);
}

function init() {
  snowflakes = [];
  resize();
  render(lastNow = performance.now());
}

window.addEventListener('resize', resize);
window.addEventListener('blur', pause);
window.addEventListener('focus', resume);

init();
