<?php

namespace App\Http\Middleware;

use App\Models\gachamaster;
use App\Models\redeemmaster;
use Closure;
use Illuminate\Http\Request;

class cekstatusgacha
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$type = null,$from = null)
    {

        // return $next($request);
        if ($type == 'saka'){
            $gacha = redeemmaster::where('id',1)->first();
        }else{
            $gacha = gachamaster::where('id',1)->first();
        }
        if ($gacha->status == 0) {
            if ($from == 'web') {
                # code...
            }else{
                $notify[] = ['error','Event Belum Dimulai.'];
                return redirect()->route('user.home')->withNotify($notify);
            }
        }

        return $next($request);

        // $notify[] = ['warning','Sorry, You have to verification data first.'];
        // return redirect()->route('user.verification')->withNotify($notify);
    }
}
