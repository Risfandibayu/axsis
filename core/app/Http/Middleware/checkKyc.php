<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkKyc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->is_kyc == 2) {
            return $next($request);
        }
        if (auth()->user()->is_kyc == 1) {
            $notify[] = ['warning','Your data on process verification.'];
            return redirect()->route('user.home')->withNotify($notify);
        }

        $notify[] = ['warning','Sorry, You have to verification data first.'];
        return redirect()->route('user.verification')->withNotify($notify);
    }
}
