<?php

namespace App\Http\Controllers;

use App\Models\usergacha;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsergachaController extends Controller
{
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $reque st
     * @return \Illuminate\Http\Response
     */
    public function filiGotIt($userid){
        $user = User::where('id', decrypt_id($userid))->firstorfail();
        $kuota = $user->gacha_qty;
        $gacha = UserGacha::latest()->get();
        $messages = $gacha->map(function($item) {
            return 'Selamat kepada ' . cekuser($item->user_id) . ' telah mendapatkan ' . cekprize($item->gachamasterdetail_id) . ' di GEBYAR DINARAN!';
        });
        return view($this->activeTemplate .'user.gacha.index2',compact('kuota','messages','userid'));
    }
    public function filiGotIt2(){
        $kuota = Auth::user()->gacha_qty;
        $gacha = UserGacha::latest()->get();
        $messages = $gacha->map(function($item) {
            return 'Selamat kepada ' . cekuser($item->user_id) . ' telah mendapatkan ' . cekprize($item->gachamasterdetail_id) . ' di GEBYAR DINARAN!';
        });
        return view($this->activeTemplate .'user.gacha.index',compact('kuota','messages'));
    }

    public function store(Request $request)
{
    if (Auth::user()->gacha_qty < 1) {
        return response()->json(['error' => 'Tiket redeem anda kosong!, Silahkan lakukan repeat order untuk mendapatkan tiket redeem',
        'links'=>route('user.plan.index')    
        ], 400);
    }

    $userId = Auth::user()->id;
    $item = performGacha($userId);

    if (!$item) {
        return response()->json(['error' => 'Empty item'], 400);
    }

    return response()->json(['message' => 'Gacha successful', 'item' => $item], 200);
}
    public function store2($id,Request $request)
{

    $user = User::where('id', decrypt_id($id))->first();
    
    if ($user->gacha_qty < 1) {
        return response()->json(['error' => 'Tiket redeem anda kosong!, Silahkan lakukan repeat order untuk mendapatkan tiket redeem',
        'links'=>route('user.plan.index')    
        ], 400);
    }

    $userId = $user->id;
    $item = performGacha($userId);

    if (!$item) {
        return response()->json(['error' => 'Empty item'], 400);
    }

    return response()->json(['message' => 'Gacha successful', 'item' => $item], 200);
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function show(usergacha $usergacha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function edit(usergacha $usergacha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usergacha $usergacha)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usergacha  $usergacha
     * @return \Illuminate\Http\Response
     */
    public function destroy(usergacha $usergacha)
    {
        //
    }
}
