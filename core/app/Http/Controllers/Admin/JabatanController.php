<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pageTitle = 'Jabatan';
        $emptyMessage = 'No Jabatan found';
        $jabatansweek = jabatan::where('type','Weekly')->paginate(getPaginate());
        $jabatansmonth = jabatan::where('type','Monthly')->paginate(getPaginate());
        return view('admin.jabatan.index', compact('pageTitle', 'jabatansweek','jabatansmonth', 'emptyMessage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
        ]);

        $jabatan = new Jabatan();
        $jabatan->name = $request->name;
        $jabatan->type = $request->type;

        if ($request->type == "Weekly") {
            # code...
            if (!$request->weekly_sp || !$request->weekly_amount || !$request->weekly_limit) {
                # code...
                $notify[] = ['error', 'Data cannot be empty'];
                return back()->withNotify($notify);
            }
            $jabatan->req = $request->weekly_sp;
            $jabatan->limit = $request->weekly_limit;
            $jabatan->amount = $request->weekly_amount;
            $jabatan->status    = $request->status?1:0;
            $jabatan->save();

            $notify[] = ['success', 'Jabatan successfully Added'];
            return back()->withNotify($notify);

        }
        if ($request->type == "Monthly") {
            # code...
            if (!$request->monthly_sp || !$request->monthly_amount || !$request->monthly_limit) {
                # code...
                $notify[] = ['error', 'Data cannot be empty'];
                return back()->withNotify($notify);
            }

            $jabatan->req = $request->monthly_sp;
            $jabatan->limit = $request->monthly_limit;
            $jabatan->amount = $request->monthly_amount;
            $jabatan->status    = $request->status?1:0;
            $jabatan->save();

            $notify[] = ['success', 'Jabatan successfully Added'];
            return back()->withNotify($notify);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function show(jabatan $jabatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function edit(jabatan $jabatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
        ]);

        $jabatan = Jabatan::where('id',$request->id)->first();
        $jabatan->name = $request->name;

        if ($jabatan->type == "Weekly") {
            # code...
            if (!$request->weekly_sp || !$request->weekly_amount || !$request->weekly_limit) {
                # code...
                $notify[] = ['error', 'Data cannot be empty'];
                return back()->withNotify($notify);
            }
            $jabatan->req = $request->weekly_sp;
            $jabatan->limit = $request->weekly_limit;
            $jabatan->amount = $request->weekly_amount;
            $jabatan->status    = $request->status?1:0;
            $jabatan->save();

            $notify[] = ['success', 'Jabatan successfully Updated'];
            return back()->withNotify($notify);

        }
        if ($jabatan->type == "Monthly") {
            # code...
            if (!$request->monthly_sp || !$request->monthly_amount || !$request->monthly_limit) {
                # code...
                $notify[] = ['error', 'Data cannot be empty'];
                return back()->withNotify($notify);
            }

            $jabatan->req = $request->monthly_sp;
            $jabatan->limit = $request->monthly_limit;
            $jabatan->amount = $request->monthly_amount;
            $jabatan->status    = $request->status?1:0;
            $jabatan->save();

            $notify[] = ['success', 'Jabatan successfully Updated'];
            return back()->withNotify($notify);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jabatan  $jabatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(jabatan $jabatan)
    {
        //
    }
}
