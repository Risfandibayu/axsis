<?php

namespace App\Http\Controllers\Admin;

use App\Models\gachamaster;
use App\Http\Controllers\Controller;
use App\Models\gachamasterdetail;
use App\Models\User;
use App\Models\usergachafix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GachamasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pageTitle = 'Gacha Master';
        $empty_message = 'No Gacha Data found';
        $gacha = gachamaster::paginate(getPaginate());
        return view('admin.gacha.index', compact('pageTitle','gacha', 'empty_message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gachamaster  $gachamaster
     * @return \Illuminate\Http\Response
     */
    public function show(gachamaster $gachamaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gachamaster  $gachamaster
     * @return \Illuminate\Http\Response
     */
    public function edit(gachamaster $gachamaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gachamaster  $gachamaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gachamaster $gachamaster)
    {
        //
        $prod = gachamaster::where('id',$request->id)->first();
        $prod->name                     = $request->name ?? $prod->name;
        $prod->reward_percent           = $request->reward_percent;
        $prod->status           = $request->status?1:0;
        $prod->save();

        // adminlog(Auth::guard('admin')->user()->id,'Update Gacha '.$prod->name);

        $notify[] = ['success', 'Gacha edited successfully'];
        return back()->withNotify($notify);
    }

    public function detail($id){
        $gachamaster = gachamaster::where('id',$id)->first();
        $pageTitle = 'Gacha Detail '.$gachamaster->name;
        $empty_message = 'No Gacha Detail Data found';
        $gacha = gachamasterdetail::where('gacha_id','=',$id)->paginate(getPaginate());

        $total = gachamasterdetail::where('gacha_id', '=', $id)
        ->selectRaw('SUM(modal * qty) as total')
        ->pluck('total')
        ->first();
        $total_qty = gachamasterdetail::where('gacha_id', '=', $id)
        ->selectRaw('SUM(qty) as total_qty')
        ->pluck('total_qty')
        ->first();

        $status_page = $gachamaster->status ?? 0;

        return view('admin.gacha.detail', compact('pageTitle','gacha','gachamaster','status_page','total','total_qty', 'empty_message'));
    }
    public function status($id){
        $prod = gachamaster::where('id',$id)->first();
        $prod->status           = 1;
        $prod->save();

        // adminlog(Auth::guard('admin')->user()->id,'Update Gacha Status'.$prod->name);

        $notify[] = ['success', 'Gacha edited successfully'];
        return back()->withNotify($notify);
    }
    public function plot($id){
        $empty_message = 'No Gacha Data found';
        $gacha = gachamasterdetail::where('id', '=', $id)->first();
        $pageTitle = 'Redeem Plot Item :'.$gacha->name;
        $userplot = usergachafix::where('gachadetail_id', '=', $id)->get();
        $user=User::where('status',1)->get();
        return view('admin.gacha.plot', compact('pageTitle','gacha', 'empty_message','userplot','user'));
    }
    public function addplotuser(Request $request){
        $plot = new usergachafix();
        $plot->gachadetail_id = $request->gacha_id;
        $plot->user_id = $request->user_id;
        $plot->qty = $request->qty;
        $plot->save();

        // adminlog(Auth::guard('admin')->user()->id,'Update Gacha Status'.$plot->gachadetail_id);

        $notify[] = ['success', 'User Add successfully'];
        return back()->withNotify($notify);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gachamaster  $gachamaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(gachamaster $gachamaster)
    {
        //
    }
}
