<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\jabatan;
use App\Models\jabatanlog;
use Illuminate\Http\Request;

class JabatanlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pageTitle = 'Jabatan History';
        $emptyMessage = 'No Jabatan Log found.';
        $jabatan = jabatanlog::orderBy('id','desc')->with('user')->with('jabatan')->paginate(getPaginate());
        return view('admin.users.jabatan_log', compact('pageTitle', 'emptyMessage', 'jabatan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jabatanlog  $jabatanlog
     * @return \Illuminate\Http\Response
     */
    public function show(jabatanlog $jabatanlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jabatanlog  $jabatanlog
     * @return \Illuminate\Http\Response
     */
    public function edit(jabatanlog $jabatanlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jabatanlog  $jabatanlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jabatanlog $jabatanlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jabatanlog  $jabatanlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(jabatanlog $jabatanlog)
    {
        //
    }
}
