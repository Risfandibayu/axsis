<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\UserExtra;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $pageTitle = 'Orders';
        $orders = Order::with('product','user')->with('detail.product')->orderBy('id','desc')->paginate(getPaginate());
        $empty_message = 'Order not found';
        return view('admin.orders',compact('pageTitle','orders','empty_message'));
    }

    public function status(Request $request,$id){
        $request->validate([
            'status'=>'required|in:1,2'
        ]);

        $order = Order::where('status',0)->with('detail')->findOrFail($id);
        // $product = $order->product;
        $user = $order->user;

        $uex = UserExtra::where('user_id', $user->id)->first();
        $uexrefid = UserExtra::where('user_id', $user->ref_id)->first();

        if($request->status == 1){
            $order->status = 1;
            $order->no_resi = "(".$request->pro.") ".$request->no_resi;
            $details = 'Orders success';
            // updateBV($user->id, $product->bv, $details);
            $template = 'order_shipped';
        }else{
            $order->status = 2;
            $user->balance += $order->total;
            $user->total_invest -= ($order->total - $order->ongkir);
            $user->save();

            $transaction = new Transaction();
            $transaction->user_id = $order->user_id;
            $transaction->amount = $order->total;
            $transaction->post_balance = $user->balance;
            $transaction->charge = 0;
            $transaction->trx_type = '+';
            $transaction->details = 'Orders cancel';
            $transaction->trx =  $order->trx;
            $transaction->save();

            // $product->quantity += $order->quantity;
            // $product->save();
            foreach ($order->detail as $item) {
    
                $product = Product::where('id',$item->product_id)->first();
                $product->quantity += $item->qty;
                $product->save();

                if (firstrx2($user->id) == true) {
                    $uexrefid->count -= $product->sp_poin;
                    $uexrefid->save();
                }else{
                    $uex->count -= $product->sp_poin;
                    $uex->save();
                }
            }

            $template = 'order_cancelled';

        }

        $order->save();

        $general = GeneralSetting::first();

        // notify($user,$template,[
        //     'product_name'=>$product->name,
        //     'quantity'=>$request->quantity,
        //     'price'=>showAmount($product->price),
        //     'total_price'=>showAmount($order->total_price),
        //     'currency'=>$general->cur_text,
        //     'trx'=>$order->trx,
        // ]);

        $notify[] = ['success','Order status updated successfully'];
        return back()->withNotify($notify);

    }
}
