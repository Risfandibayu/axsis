<?php

namespace App\Http\Controllers\Gateway;

use App\Http\Controllers\Controller;
use App\Models\AdminNotification;
use App\Models\Deposit;
use App\Models\GatewayCurrency;
use App\Models\GeneralSetting;
use App\Models\Transaction;
use App\Models\User;
use App\Rules\FileTypeValidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function __construct()
    {
        return $this->activeTemplate = activeTemplate();
    }

    public function deposit()
    {
        $gatewayCurrency = GatewayCurrency::whereHas('method', function ($gate) {
            $gate->where('status', 1);
        })->with('method')->orderby('method_code')->get();
        $pageTitle = 'Deposit Methods';
        return view($this->activeTemplate . 'user.payment.deposit', compact('gatewayCurrency', 'pageTitle'));
    }

    public function depositInsert(Request $request)
    {

        $request->validate([
            'amount' => 'required|numeric|gt:0',
            'method_code' => 'required',
            'currency' => 'required',
        ]);


        $user = auth()->user();
        $gate = GatewayCurrency::whereHas('method', function ($gate) {
            $gate->where('status', 1);
        })->where('method_code', $request->method_code)->where('currency', $request->currency)->first();
        if (!$gate) {
            $notify[] = ['error', 'Invalid gateway'];
            return back()->withNotify($notify);
        }

        if ($gate->min_amount > $request->amount || $gate->max_amount < $request->amount) {
            $notify[] = ['error', 'Please follow deposit limit'];
            return back()->withNotify($notify);
        }

        $charge = $gate->fixed_charge + ($request->amount * $gate->percent_charge / 100);
        $payable = $request->amount + $charge;
        $final_amo = $payable * $gate->rate;

        $data = new Deposit();
        $data->user_id = $user->id;
        $data->method_code = $gate->method_code;
        $data->method_currency = strtoupper($gate->currency);
        $data->amount = $request->amount;
        $data->charge = $charge;
        $data->rate = $gate->rate;
        $data->final_amo = $final_amo;
        $data->btc_amo = 0;
        $data->btc_wallet = "";
        $data->trx = getTrx();
        $data->try = 0;
        $data->status = 0;
        $data->paylabs_va = $request->va;
        $data->save();
        session()->put('Track', $data->trx);
        return redirect()->route('user.deposit.preview');
    }


    public function depositPreview()
    {

        $track = session()->get('Track');
        $data = Deposit::where('trx', $track)->where('status',0)->orderBy('id', 'DESC')->firstOrFail();
        $pageTitle = 'Payment Preview';
        return view($this->activeTemplate . 'user.payment.preview', compact('data', 'pageTitle'));
    }


    public function depositConfirm()
    {
        $track = session()->get('Track');
        $deposit = Deposit::where('trx', $track)->where('status',0)->orderBy('id', 'DESC')->with('gateway')->firstOrFail();
        
        if ($deposit->method_code >= 1000) {
            $this->userDataUpdate($deposit);
            $notify[] = ['success', 'Your deposit request is queued for approval.'];
            return back()->withNotify($notify);
        }


        $dirName = $deposit->gateway->alias;
        $new = __NAMESPACE__ . '\\' . $dirName . '\\ProcessController';

        $data = $new::process($deposit);
        $data = json_decode($data);


        if (isset($data->error)) {
            $notify[] = ['error', $data->message];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        if (isset($data->redirect)) {
            return redirect($data->redirect_url);
        }

        // for Stripe V3
        if(@$data->session){
            $deposit->btc_wallet = $data->session->id;
            $deposit->save();
        }

        $pageTitle = 'Payment Confirm';
        return view($this->activeTemplate . $data->view, compact('data', 'pageTitle', 'deposit'));
    }


    public static function userDataUpdate($trx)
    {
        $general = GeneralSetting::first();
        $data = Deposit::where('trx', $trx)->first();
        if ($data->status == 0) {
            $data->status = 1;
            $data->save();

            $user = User::find($data->user_id);
            $user->balance += $data->amount;
            $user->save();

            $transaction = new Transaction();
            $transaction->user_id = $data->user_id;
            $transaction->amount = $data->amount;
            $transaction->post_balance = $user->balance;
            $transaction->charge = $data->charge;
            $transaction->trx_type = '+';
            $transaction->details = 'Deposit Via ' . $data->gatewayCurrency()->name;
            $transaction->trx = $data->trx;
            $transaction->save();

            $adminNotification = new AdminNotification();
            $adminNotification->user_id = $user->id;
            $adminNotification->title = 'Deposit successful via '.$data->gatewayCurrency()->name;
            $adminNotification->click_url = urlPath('admin.deposit.successful');
            $adminNotification->save();

            notify($user, 'DEPOSIT_COMPLETE', [
                'method_name' => $data->gatewayCurrency()->name,
                'method_currency' => $data->method_currency,
                'method_amount' => showAmount($data->final_amo),
                'amount' => showAmount($data->amount),
                'charge' => showAmount($data->charge),
                'currency' => $general->cur_text,
                'rate' => showAmount($data->rate),
                'trx' => $data->trx,
                'post_balance' => showAmount($user->balance)
            ]);


        }
    }

    // public function manualDepositConfirm()
    // {
    //     $track = session()->get('Track');
    //     $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
    //     if (!$data) {
    //         return redirect()->route(gatewayRedirectUrl());
    //     }
    //     if ($data->method_code > 999) {

    //         $pageTitle = 'Deposit Confirm';
    //         $method = $data->gatewayCurrency();
    //         return view($this->activeTemplate . 'user.manual_payment.manual_confirm', compact('data', 'pageTitle', 'method'));
    //     }
    //     abort(404);
    // }

    public function manualDepositUpdate(Request $request)
    {
        $track = session()->get('Track');
        $data = Deposit::with('gateway')->where('status', 0)->where('trx', $track)->first();
        if (!$data) {
            return redirect()->route(gatewayRedirectUrl());
        }

        $params = json_decode($data->gatewayCurrency()->gateway_parameter);

        $rules = [];
        $inputField = [];
        $verifyImages = [];

        if ($params != null) {
            foreach ($params as $key => $custom) {
                $rules[$key] = [$custom->validation];
                if ($custom->type == 'file') {
                    array_push($rules[$key], 'image');
                    array_push($rules[$key], new FileTypeValidate(['jpg','jpeg','png']));
                    array_push($rules[$key], 'max:2048');

                    array_push($verifyImages, $key);
                }
                if ($custom->type == 'text') {
                    array_push($rules[$key], 'max:191');
                }
                if ($custom->type == 'textarea') {
                    array_push($rules[$key], 'max:300');
                }
                $inputField[] = $key;
            }
        }
        $this->validate($request, $rules);


        $directory = date("Y")."/".date("m")."/".date("d");
        $path = imagePath()['verify']['deposit']['path'].'/'.$directory;
        $collection = collect($request);
        $reqField = [];
        if ($params != null) {
            foreach ($collection as $k => $v) {
                foreach ($params as $inKey => $inVal) {
                    if ($k != $inKey) {
                        continue;
                    } else {
                        if ($inVal->type == 'file') {
                            if ($request->hasFile($inKey)) {
                                try {
                                    $reqField[$inKey] = [
                                        'field_name' => $directory.'/'.uploadImage($request[$inKey], $path),
                                        'type' => $inVal->type,
                                    ];
                                } catch (\Exception $exp) {
                                    $notify[] = ['error', 'Could not upload your ' . $inKey];
                                    return back()->withNotify($notify)->withInput();
                                }
                            }
                        } else {
                            $reqField[$inKey] = $v;
                            $reqField[$inKey] = [
                                'field_name' => $v,
                                'type' => $inVal->type,
                            ];
                        }
                    }
                }
            }
            $data->detail = $reqField;
        } else {
            $data->detail = null;
        }



        $data->status = 2; // pending
        $data->save();


        $adminNotification = new AdminNotification();
        $adminNotification->user_id = $data->user->id;
        $adminNotification->title = 'Deposit request from '.$data->user->username;
        $adminNotification->click_url = urlPath('admin.deposit.details',$data->id);
        $adminNotification->save();

        $general = GeneralSetting::first();
        notify($data->user, 'DEPOSIT_REQUEST', [
            'method_name' => $data->gatewayCurrency()->name,
            'method_currency' => $data->method_currency,
            'method_amount' => showAmount($data->final_amo),
            'amount' => showAmount($data->amount),
            'charge' => showAmount($data->charge),
            'currency' => $general->cur_text,
            'rate' => showAmount($data->rate),
            'trx' => $data->trx
        ]);

        $notify[] = ['success', 'You have deposit request has been taken.'];
        return redirect()->route('user.deposit.history')->withNotify($notify);
    }


    public function manualDepositConfirm()
    {
        date_default_timezone_set('Asia/Jakarta'); // Set zona waktu sesuai dengan kebutuhan Anda

        list($microseconds, $seconds) = explode(' ', microtime());
        $milliseconds = sprintf('%03d', round($microseconds * 1000));
        $formatted_date = date('Y-m-d\TH:i:s.', $seconds) . $milliseconds . date('P', $seconds);

        $track = session()->get('Track');
        $data = Deposit::where('status', 0)->where('trx', $track)->first();
        if (!$data) {
            return redirect()->route(gatewayRedirectUrl());
        }
        if ($data->status != 0) {
            return redirect()->route(gatewayRedirectUrl());
        }

        $user = user::where('id',$data->user_id)->first();

        // Contoh data
        $httpMethod = "POST";
        $endpointUrl = "/payment/v2/h5/createLink";
        $timestamp = $formatted_date; // Misalnya timestamp dari header
        $mid = env('MID');
        $trxid = $data->id;
        $body = array(
            "merchantId" => $mid,
            "merchantTradeNo" => $data->trx,
            "requestId" => $trxid,
            "amount" => intval($data->final_amo),
            "productName" => "Deposit",
            "payer" => $user->username,
            "phoneNumber" => $user->mobile,
            "notifyUrl" => route('deposit.manual.callback'),
            "redirectUrl" => route(gatewayRedirectUrl()),
        ); // Contoh body JSON
        $privateKeyPath = base_path('private-key.pem');
        $privateKey = file_get_contents($privateKeyPath);

        // Langkah-langkah:
        // 1. Minify body JSON
        $minifiedBody = minifyJsonBody(json_encode($body));

        // 2. Membuat stringContent
        $stringContent = createStringContent($httpMethod, $endpointUrl, $minifiedBody, $timestamp);

        // 3. Membuat signature
        $signature = createSignature($stringContent, $privateKey);

        // Hasil signature



        $data_string = json_encode($body);

        // URL tujuan permintaan
        // $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl;
        if (env('APP_ENV') != 'production') {
            $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl; // for development mode
        }else{
            $url = 'https://pay.paylabs.co.id'.$endpointUrl; // for production mode
        }

        // Konfigurasi cURL
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=utf-8',
            'X-TIMESTAMP: '.$timestamp,
            'X-SIGNATURE: '.$signature,
            'X-PARTNER-ID: '.$mid,
            'X-REQUEST-ID: '.$trxid
            )
        );

        // Eksekusi permintaan dan ambil respons
        $result = curl_exec($ch);

        // Cek jika terjadi kesalahan
        if(curl_errno($ch)) {
            echo 'Error: ' . curl_error($ch);
        }

        // Tutup koneksi cURL
        curl_close($ch);

        $response = json_decode($result,true);


        // echo "timestamp: ". $formatted_date."<br><br>";
        // echo "url : ".$url."<br><br>";
        // echo "body : ".$minifiedBody."<br><br>";
        // echo "stringContent : ".$stringContent."<br><br>";
        // echo 'X-TIMESTAMP : '.$timestamp."<br>";
        // echo "X-SIGNATURE : $signature"."<br>";
        // echo 'X-PARTNER-ID : '.$mid."<br>";
        // echo 'X-REQUEST-ID : '.$trxid."<br><br>";
        // echo "response : ".$result;
        // dd(json_decode($result,true));
        if ($response['errCode'] == "0") {
            # code...
            // $time = $response['expiredTime'];

            // // Memecah data waktu menjadi bagian-bagian yang relevan
            // $year = substr($time, 0, 4);
            // $month = substr($time, 4, 2);
            // $day = substr($time, 6, 2);
            // $hour = substr($time, 8, 2);
            // $minute = substr($time, 10, 2);
            // $second = substr($time, 12, 2);

            // // Format data waktu ke dalam format yang sesuai dengan tipe kolom datetime (YYYY-MM-DD HH:MM:SS)
            // $expireTime = "$year-$month-$day $hour:$minute:$second";

            $data->status = 2;
            // $data->paylabs_exptime = $expireTime;
            // $data->paylabs_platformtradeno = $response['platformTradeNo'];
            // $data->paylabs_productname = $response['productName'];
            // $data->paylabs_va = $response['productName'];
            $data->paylabs_url = $response['url'];
            $data->save(); 
            return redirect()->to($data->paylabs_url);
            // dd($response);
            // return redirect()->route('user.deposit.payment', $data->trx);

        }else{
            $notify[] = ['error', 'Error '.$response['errCode']];
            return redirect()->route('user.deposit')->withNotify($notify);
        }

    }

    public function payment($trx){
        $depo = deposit::where('trx','=',$trx)->first();
        if (!$depo) {
            # code...
            $notify[] = ['error', 'Invalid deposit ID, please try again.'];
            return back()->withNotify($notify);
        }
        if ($depo->user_id != Auth::user()->id) {
            # code...
            $notify[] = ['error', 'Invalid deposit ID, please try again.'];
            return back()->withNotify($notify);
        }
        $pageTitle = 'Payment Page';
        // $method = $data->gateway_currency();
        return view($this->activeTemplate . 'user.payment.payment', compact('depo','pageTitle'));
    }

    public function callback(Request $request){
        
        $status = $request->status;
        $data = Deposit::where('trx', $request->merchantTradeNo)->first();
        if ($data) {
            if ($status == '02') {
                $this->userDataUpdate2($data->trx);
                return response()->json(['status'=> $status]);
            }elseif($status == '01'){
                return response()->json(['status'=> $status]);
            }
        }

        // }else{
        //     $data->status = 3;
        //     $data->admin_feedback =  "Deposit Canceled";
        //     $data->save();

        //     return response()->json(['status'=> $status]);
        // }
    }

    public static function userDataUpdate2($trx)
    {
        $gnl = GeneralSetting::first();
        $data = Deposit::where('trx', $trx)->first();
        if ($data->status == 2) {
            $data->status = 1;
            $data->save();

            $user = User::find($data->user_id);
            $user->balance += $data->amount;
            $user->save();
            $gateway = $data->gateway;

            $transaction = new Transaction();
            $transaction->user_id = $data->user_id;
            $transaction->amount = $data->amount;
            $transaction->post_balance = getAmount($user->balance);
            $transaction->charge = getAmount($data->charge);
            $transaction->trx_type = '+';
            $transaction->details = 'Deposit Via ' . $data->gateway_currency()->name;
            $transaction->trx = $data->trx;
            $transaction->save();

            $adminNotification = new AdminNotification();
            $adminNotification->user_id = $user->id;
            $adminNotification->title = 'Deposit successful via '.$data->gateway_currency()->name;
            $adminNotification->click_url = route('admin.deposit.successful');
            $adminNotification->save();



        }
    }
}
