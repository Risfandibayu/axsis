<?php

namespace App\Http\Controllers;

use App\Models\alamat;
use App\Models\cart;
use App\Models\GeneralSetting;
use App\Models\Order;
use App\Models\orderdetail;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserExtra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'qty'=>'required|integer|gt:0',
            'product_id'=>'required|integer|gt:0',
        ]);
        $user = auth()->user();
        $existcart = cart::where('product_id', $request->product_id)->where('user_id', $user->id)->first();
        $product = Product::where('id',$request->product_id)->first();
        if ($request->qty > $product->quantity){
            $notify[] = ['error', 'Product stock exceeds the quantity you entered'];
            return back()->withNotify($notify);
        }
        if ($existcart) {
            # code...
            if (($request->qty + $existcart->qty) > $product->quantity){
                $notify[] = ['error', 'Product stock exceeds the quantity you entered'];
                return back()->withNotify($notify);
        }
        }
        

        if ($existcart) {
            $existcart->user_id = $user->id;
            $existcart->product_id = $request->product_id;
            $existcart->referral = $request->referral;
            $existcart->qty += $request->qty;
            $existcart->save();
        }else{
            $cart = new Cart();
            $cart->user_id = $user->id;
            $cart->product_id = $request->product_id;
            $cart->referral = $request->referral;
            $cart->qty = $request->qty;
            $cart->save();
        }

        // return response()->json(['message' => 'Add to cart success!!']);
        $notify[] = ['success', 'Success'];
        return back()->withNotify($notify);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(cart $cart)
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(cart $cart)
    {
        //
    }


    public function cartuser(){
        $user = auth()->user();
        $cart = cart::where('user_id', $user->id)->with('product')->paginate(getPaginate());
        $alamat = alamat::where('user_id', $user->id)->get();
        $pageTitle = "Carts";
        $empty_message = 'Carts Empty';
        return view($this->activeTemplate . '.user.carts',compact('pageTitle','user','cart','alamat','empty_message'));
    }

    public function delCart(Request $request, $id){
        $cart = cart::where('id',$request->id)->delete();

        $notify[] = ['success', 'Item Deleted Succesfully'];
        return back()->withNotify($notify);
    }

    public function checkOut(Request $request){
        // dd($request->all());


        $user = Auth::user();
        $index = 1;
        $berattotal = 0;
        $totalakhir = 0;
        $alamat = alamat::where('id', $request->alamat)->first();
        
        if (!$alamat->kode_pos) {
            # code...
            $notify[] = ['error', 'Incomplete address, please complete the recipient`s address first'];
            return redirect()->back()->withNotify($notify);
            
        }
        if (!$request->id) {
            # code...
            $notify[] = ['error', 'The Shipping Cart Is Empty'];
            return redirect()->back()->withNotify($notify);
            
        }
        session()->put('Alamat', $alamat);

        foreach($request->id as $id){
            $cart = cart::where('id',$id)->with('product')->first();
            $gold = product::where('id',$cart->product->id)->first();

            if ($request->quant[$index] > $gold->quantity ) {
                # code...
                $notify[] = ['error', 'The qty you input exceeds the amount of gold you have'];
                return redirect()->back()->withNotify($notify);
            }
            $berat = $cart->product->weight * $cart->qty;
            $total = $cart->product->price * $cart->qty;
                # code...
            $berattotal = $berattotal + $berat;
            $totalakhir = $totalakhir + $total;
            
            $index++;   
        }
        session()->put('Product', $request->id);
        session()->put('Qty', $request->quant);

        
        $options = [
            'cache_wsdl'     => WSDL_CACHE_NONE,
            'trace'          => 1,
            'stream_context' => stream_context_create(
                [
                    'ssl' => [
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                        'allow_self_signed' => true
                    ]
                ]
            )
        ];

        try {
        $wsdl="http://api.rpxholding.com/wsdl/rpxwsdl.php?wsdl";
        libxml_disable_entity_loader(false);
        $client = new \SoapClient($wsdl,$options);
        $username = env('USER_RPX');
        $password  = env('PASS_RPX');
        $format = 'json';

        
            $result = $client->getRatesPostalCode($username,$password,env('POSCODE_RPX'),$alamat->kode_pos,'RGP',$berattotal/1000,'0',$format);
            $response = json_decode($result,true);
            // dd($response);
            if (!$response) {
                # code...
                $notify[] = ['error', 'Invalid destination address'];
                return redirect()->back()->withNotify($notify);
            }else{
                // dd((int)$response['RPX']['DATA']['PRICE'] - 20000);
                // if ((int)$response['RPX']['DATA']['PRICE'] > 20000) {
                //     # code...
                    // $ongkir = (int)$response['RPX']['DATA']['PRICE'] - 20000;
                    // $ongkir = (int)$response['RPX']['DATA']['PRICE'];
                // }else{
                    // $ongkir = 0;
                    
                // }
                if ($alamat->kode_pos == env('POSCODE_RPX')) {
                    # code...
                    $ongkir = 0;
                }else{
                    $ongkir = (int)$response['RPX']['DATA']['PRICE'];
                }

                // if($user->balance < $ongkir){
                //     $notify[] = ['error', 'Your balance is not enough for postage payment'];
                //     return redirect()->back()->withNotify($notify);
                // }else{

                    session()->put('Ongkir', $ongkir);
                    // $notify[] = ['success', 'Gold delivery request is successful, please wait for confirmation.'];
                    // return redirect()->back()->withNotify($notify);
                // }
            }
        }
        catch ( \Exception $e ) {
            // echo $e->getMessage();
            $notify[] = ['error', $e->getMessage()];
            return redirect()->back()->withNotify($notify);
        }
        // dd($ongkir);

        return redirect()->route('user.cart.preview');

    }

    public function checkOutPreview(){
        // dd(session()->get('Qty'));
        $qty = session()->get('Qty');
        $index = 1;
        $berattotal = 0;
        $keptotal = 0;
        $total = 0;

        $alamat = session()->get('Alamat');
        $ongkir = session()->get('Ongkir');
        // dd($qty[1]);
        $pageTitle = 'Cart Checkout Preview';

        foreach(session()->get('Product') as $id){
            $cart = cart::where('id',$id)->with('product')->first();
            if($qty[$index] != $cart->qty){
                $cart->qty = $qty[$index];
                $cart->save();
            }

            $price = (firstrx2(Auth::user()->id)) ? $cart->product->price_public : $cart->product->price;

            $berat = $cart->product->weight * $cart->qty;
            $hargatotal = $price * $cart->qty;
                # code...
            $berattotal = $berattotal + $berat;
            $total = $total + $hargatotal;
            $keping = $cart->qty;
            $keptotal = $keptotal + $keping;
            $index++;  
        }
        // dd($keptotal);
        

        foreach(session()->get('Product') as $id){
            $cartlist[] = cart::where('id',$id)->with('product')->first();
            // $gold[] = Gold::where('id',$dcart->gold->id)->first();  
        }

        
        // dd($alamat);
        return view($this->activeTemplate . 'user.cartpreview', compact('pageTitle','cartlist','berattotal','keptotal','alamat','ongkir','total'));
    }

    public function checkOutConfirm(Request $request){
        $user = Auth::user();
        $alamat = alamat::where('id', $request->alamat)->first();
        $order = new Order();
        $order->trx = getTrx();
        $order->user_id = $user->id;
        $order->alamat = $alamat->alamat;
        $order->nama_penerima = $alamat->nama_penerima;
        $order->no_telp_penerima = $alamat->no_telp;
        $order->kode_pos = $alamat->kode_pos;
        $order->status = 0;
        $order->ongkir = $request->ongkir;
        $order->total = $request->total;
        
        if($request->payment=='balance'){
            $order->payment_status = 1;
            $order->payment_method = $request->payment;
            $order->save();
            $user->balance -=  $request->total;
            $user->total_invest += ($request->total - $request->ongkir);
            $user->save();

            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $request->total;
            $transaction->post_balance = $user->balance;
            $transaction->charge = 0;
            $transaction->trx_type = '-';
            $transaction->details = 'item purchase';
            $transaction->trx =  getTrx();
            $transaction->save();

            $uex = UserExtra::where('user_id', $user->id)->first();
            $uexrefid = UserExtra::where('user_id', $user->ref_id)->first();

            $cart = cart::where('user_id',$user->id)->get();

            if (count($cart) == 0) {
                # code...
                $notify[] = ['error', 'Your delivery cart is empty. Please add product to cart first!!'];
                return redirect()->route('user.gold.invest')->withNotify($notify);
            }

            foreach ($cart as $item) {
                $sgdet = new orderdetail();
                $sgdet->order_id = $order->id;
                $sgdet->product_id = $item->product_id;
                $sgdet->qty = $item->qty;
                $sgdet->save();

                $product = Product::where('id',$item->product_id)->first();
                $product->quantity -= $item->qty;
                $product->save();

                if ($product->is_multilevel == 1) {
                    $price = (firstrx2(Auth::user()->id)) ? $product->price_public : $product->price;

                    refTreeComission($user->id,$price * $item->qty,$item->qty ,'Sales Commission');
                }

                if(firstrx3($user->id) == 1) {
                    $uexrefid->count += $product->sp_poin * $item->qty;
                    $uexrefid->save();
                }else if(firstrx3($user->id) > 1) {
                    $uex->count += $product->sp_poin * $item->qty;
                    $uex->save();
                }
            }

            $cart->each->delete();



            // dd($sg);
            $notify[] = ['success', 'Order Succesfully'];
            return redirect()->route('user.orders')->withNotify($notify);

        }

        if ($request->payment == 'other') {
            # code...
            $order->save(); 
            date_default_timezone_set('Asia/Jakarta'); // Set zona waktu sesuai dengan kebutuhan Anda
            list($microseconds, $seconds) = explode(' ', microtime());
            $milliseconds = sprintf('%03d', round($microseconds * 1000));
            $formatted_date = date('Y-m-d\TH:i:s.', $seconds) . $milliseconds . date('P', $seconds);
            $httpMethod = "POST";
            $endpointUrl = "/payment/v2/h5/createLink";
            $timestamp = $formatted_date; // Misalnya timestamp dari header
            $mid = env('MID');
            // $data = $order;
            $trxid = $order->id;
            $body = array(
                "merchantId" => $mid,
                "merchantTradeNo" => $order->trx,
                "requestId" => $trxid,
                "amount" => intval($order->total),
                "productName" => "Order",
                "payer" => $user->username,
                "phoneNumber" => $user->mobile,
                "notifyUrl" => route('order.manual.callback'),
                "redirectUrl" => route('user.orders'),
            );
            $privateKeyPath = base_path('private-key.pem');
            $privateKey = file_get_contents($privateKeyPath);

            $minifiedBody = minifyJsonBody(json_encode($body));
            $stringContent = createStringContent($httpMethod, $endpointUrl, $minifiedBody, $timestamp);
            $signature = createSignature($stringContent, $privateKey);

            $data_string = json_encode($body);

            if (env('APP_ENV') != 'production') {
                $url = 'https://sit-pay.paylabs.co.id'.$endpointUrl; // for development mode
            }else{
                $url = 'https://pay.paylabs.co.id'.$endpointUrl; // for production mode
            }
    

            // Konfigurasi cURL
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json;charset=utf-8',
                'X-TIMESTAMP: '.$timestamp,
                'X-SIGNATURE: '.$signature,
                'X-PARTNER-ID: '.$mid,
                'X-REQUEST-ID: '.$trxid
                )
            );

            // Eksekusi permintaan dan ambil respons
            $result = curl_exec($ch);

            // Cek jika terjadi kesalahan
            if(curl_errno($ch)) {
                echo 'Error: ' . curl_error($ch);
            }

            // Tutup koneksi cURL
            curl_close($ch);

            $response = json_decode($result,true);
            if ($response['errCode'] == "0") {

                $cart = cart::where('user_id',$user->id)->get();

                // if (count($cart) == 0) {
                //     # code...
                //     $notify[] = ['error', 'Your delivery cart is empty. Please add product to cart first!!'];
                //     return redirect()->route('user.gold.invest')->withNotify($notify);
                // }

                // $order = $data;

                foreach ($cart as $item) {
                    $sgdet = new orderdetail();
                    $sgdet->order_id = $order->id;
                    $sgdet->product_id = $item->product_id;
                    $sgdet->qty = $item->qty;
                    $sgdet->save();
                }

                $cart->each->delete();

                $order->payment_method = $request->payment;
                $order->payment_status = 2;
                $order->url = $response['url'];
                $order->save(); 
                return redirect()->to($order->url);
                // echo "timestamp: ". $formatted_date."<br><br>";
                // echo "url : ".$url."<br><br>";
                // echo "body : ".$minifiedBody."<br><br>";
                // echo "stringContent : ".$stringContent."<br><br>";
                // echo 'X-TIMESTAMP : '.$timestamp."<br>";
                // echo "X-SIGNATURE : $signature"."<br>";
                // echo 'X-PARTNER-ID : '.$mid."<br>";
                // echo 'X-REQUEST-ID : '.$trxid."<br><br>";
                // echo "response : ".$result;

            }else{
                $notify[] = ['error', 'Error '.$response['errCodeDes']];
                return redirect()->route('user.cartUser')->withNotify($notify);
            }
        }
    }

    public function callbackGet(Request $request){
        
        $status = $request->status;
        $data = Order::where('trx', $request->merchantTradeNo)->first();
        if ($data) {
            if ($status == '02') {
                # code...
                // $this->userDataUpdate2($data->trx);
                $data->payment_status = 1;
                $data->save();
                return response()->json(['status'=> $status]);
            }elseif($status == '01'){
                
                return response()->json(['status'=> $status]);
            }
        }
    }

    public function callback(Request $request){
        
        $status = $request->status;
        $data = Order::where('trx', $request->merchantTradeNo)->first();
        if ($data) {
            if ($status == '02') {
                # code...
                $this->userDataUpdate2($data->trx);
                // $data->payment_status = 1;
                // $data->save();
                return response()->json(['status'=> $status]);
            }elseif($status == '01'){
                
                return response()->json(['status'=> $status]);
            }
        }

        // }else{
        //     $data->status = 3;
        //     $data->admin_feedback =  "Deposit Canceled";
        //     $data->save();

        //     return response()->json(['status'=> $status]);
        // }
    }

    public static function userDataUpdate2($trx)
    {
        $gnl = GeneralSetting::first();
        $data = Order::where('trx', $trx)->first();
        if ($data->payment_status == 2) {
            $data->payment_status = 1;
            $data->save();

            $user = User::where('id', $data->user_id)->first();

            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->amount = $data->total;
            $transaction->post_balance = $user->balance;
            $transaction->charge = 0;
            $transaction->trx_type = '-';
            $transaction->details = 'item purchase';
            $transaction->trx =  getTrx();
            $transaction->save();

            $order = orderdetail::where('order_id',$data->id)->get();

            foreach ($order as $item){
                $product = Product::where('id',$item->product_id)->first();
                $product->quantity -= $item->qty;
                $product->save();

                if ($product->is_multilevel == 1) {
                    $price = (firstrx2($user->id)) ? $product->price_public : $product->price;

                    refTreeComission($user->id,$price * $item->qty,$item->qty ,'Sales Commission');
                }
            }
        }
    }
}
