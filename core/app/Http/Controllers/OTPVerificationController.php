<?php

namespace App\Http\Controllers;

use App\Models\OTPVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OTPVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OTPVerification  $oTPVerification
     * @return \Illuminate\Http\Response
     */
    public function show(OTPVerification $oTPVerification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OTPVerification  $oTPVerification
     * @return \Illuminate\Http\Response
     */
    public function edit(OTPVerification $oTPVerification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OTPVerification  $oTPVerification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OTPVerification $oTPVerification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OTPVerification  $oTPVerification
     * @return \Illuminate\Http\Response
     */
    public function destroy(OTPVerification $oTPVerification)
    {
        //
    }

    public function sendOTP()
    {
        $user = Auth::user();
        $otp = rand(100000, 999999);
    
        $otpVerification = new OTPVerification();
        $otpVerification->user_id = $user->id;
        $otpVerification->otp = $otp;
        $otpVerification->save();
    
        // Kode untuk mengirimkan OTP ke pengguna (misalnya melalui SMS atau email)
        // Contoh: Kirim OTP melalui email
        // Mail::to($user->email)->send(new OTPMail($otp));
        sendEmail($user, 'EVER_CODE_PRODUCT',[
            'code' => $otp
        ]);
    
        return response()->json(['message' => 'OTP sent successfully, Please check your inbox or spam folder in your email and enter the OTP code in the form above']);
    }
}
