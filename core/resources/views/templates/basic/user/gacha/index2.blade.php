<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>GEBYAR DINARAN</title>
    <link rel="shortcut icon" type="image/png" href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Tiny5&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
        integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('assets/gacha/style.css')}}">

    <style>
        .running-text {
            background-color: #ffae00;
            padding: 10px 0;
            text-align: center;
            font-size: 26px;
            color: #000000;
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
            z-index: -999;
        }

        <?php
        foreach ($messages as $index => $message) {
            $animationName = "moveBubble" . $index;
            $animationDuration = rand(10, 20) . "s";
            $delay = rand(0, 10) . "s"; // Random delay between 0 and 10 seconds
            $translateX1 = rand(-50, 50) . "px";
            $translateX2 = rand(-50, 50) . "px";
            $translateX3 = rand(-50, 50) . "px";
            $translateX4 = rand(-50, 50) . "px";
            echo "
                @keyframes $animationName {
                    0% {
                        transform: translateY(0) translateX(0) scale(1);
                        opacity: 1;
                    }
                    25% {
                        transform: translateY(-30vh) translateX($translateX1) scale(0.8);
                    }
                    50% {
                        transform: translateY(-90vh) translateX($translateX2) scale(0.6);
                    }
                    75% {
                        transform: translateY(-120vh) translateX($translateX3) scale(0.4);
                    }
                    100% {
                        transform: translateY(-150vh) translateX($translateX4) scale(0.5);
                        opacity: 0;
                    }
                }
                .bubble-$index {
                    animation: $animationName $animationDuration infinite;
                    animation-delay: $delay;
                }
            ";
        }
        ?>

        <?php foreach ($messages as $index => $message): ?>
        .bubble-<?php echo $index; ?> {
            left: <?php echo ($index * 20) % 100; ?>%; /* Atur posisi horizontal berdasarkan indeks */
            transform: translateX(-<?php echo ($index * 20) % 100; ?>%);
            animation: moveBubble<?php echo $index; ?> <?php echo rand(20, 40); ?>s infinite <?php echo $index * 2; ?>s;
        }
    <?php endforeach; ?>
.bubble-container {
            position: relative;
            width: 100%;
            height: 100vh;
            overflow: hidden;
        }

        .bubble {
            position: absolute;
            bottom: -150px;
            width: 150px;
            height: 150px;
            background: rgba(255, 255, 255, 0.7);
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            padding: 10px;
            box-sizing: border-box;
        }

        .bubble:nth-child(2) {
            width: 160px;
            height: 160px;
        }

        .bubble:nth-child(3) {
            width: 170px;
            height: 170px;
        }

        .bubble:nth-child(4) {
            width: 180px;
            height: 180px;
        }

        .bubble:nth-child(5) {
            width: 190px;
            height: 190px;
        }

        .bubble-text {
            color: #000;
            font-size: 14px;
            line-height: 1.5;
            word-wrap: break-word;
        }

        .section {
            width: 25vw;
            height: 100vh;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(3, 1fr);
            gap: 10px;
            padding: 10px;
            z-index: 0;
            /* Atur z-index sesuai kebutuhan */
        }

        .section div {
            background: rgba(255, 241, 241, 0.296);
            border-radius: 5px;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            padding: 10px;
            color: #000;
        }

        .bubble-container {
            position: relative;
            width: 100%;
            height: 100vh;
            overflow: hidden;
            z-index: 0;
            /* Atur z-index sesuai kebutuhan, bisa lebih rendah dari .section */
        }

        .left-c {
            position: relative;
            width: 25%;
            height: 100vh;
            padding: 15px;
            overflow: hidden;
            z-index: 0;
            display: flex;
            justify-content: space-between;
            align-items: flex-start;
            background-color: #28533a;
        }

        .right-c {
            position: relative;
            width: 25%;
            height: 100vh;
            padding: 10px;
            overflow: hidden;
            z-index: 0;
            display: flex;
            justify-content: space-between;
            align-items: flex-start;
            background-color: #28533a;
        }

        .h-container {
            display: flex;
            justify-content: space-between;
        }

        .left-c {
            display: grid;
            /* grid-template-columns: repeat(2, 1fr); */
            /* grid-template-rows: repeat(3, auto); */
            /* gap: 10px; Adjust gap as needed */
        }

        .right-c {
            display: inline;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(3, auto);
            gap: 10px;
            /* Adjust gap as needed */
        }

        .box-l {
            width: 100px;
            height: 100px;
            background-color: rgba(255, 255, 255, 0.781);
            justify-content: center;
            align-items: center;
            border-radius: 10px;
            margin: 20px 7px;
        }

        .box-r {
            width: 220px;
            height: 60px;
            background-color: rgba(255, 255, 255, 0.781);
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 10px;
            margin: 20px 20px;
        }

        .box-l img {
            max-width: 100%;
            height: auto;
        }

        .box-l p {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 16px;
            text-align: center;
            margin-bottom: 0;
            padding: 0.25em 0.4em;
            color: #fff;
            background-color: #ff5213d3;
            /* Bootstrap primary color */
            border-radius: 0.55rem;
        }

        .box-r img {
            max-width: 100%;
        }

        @media (max-width: 999px) {

            /* Changed from 768px to 767px */
            .left-c,
            .right-c {
                display: none !important;
            }

            .box-l {
                display: none !important;
            }

            .box-r {
                display: none !important;
            }

            .bubble-container {
                flex-grow: 1;
                justify-content: flex-start;
            }
        }

        .image-container {
            position: relative;
            display: inline-block;
        }

        .image-container img {
            display: block;
            width: 100%;
            height: auto;
        }

        .overlay-text {
            position: absolute;
            top: 85%;
            /* Posisikan di tengah vertikal */
            left: 50%;
            /* Posisikan di tengah horizontal */
            transform: translate(-50%, -50%);
            /* Centang teks secara sempurna */
            color: white;
            /* Warna teks */
            background-color: rgb(255, 195, 84);
            /* Latar belakang transparan */
            padding: 5px 10px;
            font-size: 16px;
            /* Atur ukuran teks sesuai keinginan */
            border-radius: 5px;
            /* Sudut melengkung opsional */
        }

        .card {
            justify-content: center;
            height: 100%;
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, .125);
            border-radius: .25rem;
        }
    </style>


</head>

<body>
    {{-- <div class="section">
        <div>Box 1</div>
        <div>Box 2</div>
        <div>Box 3</div>
        <div>Box 4</div>
        <div>Box 5</div>
        <div>Box 6</div>
    </div> --}}
    <div class="coins">
        <i class="fa fa-ticket"></i> <span id="kuota">{{$kuota}}</span> Tiket
    </div>
    <div class="media">
        <button id="play-button"><i class="fa fa-play"></i></button>
    </div>
    <audio id="curtain-sound" autoplay>
        <source src="{{asset('assets/gacha/sound/tirai.mp3')}}" type="audio/mpeg">
        Your browser does not support the audio tag.
    </audio>
    <audio id="bg-sound" loop>
        <source src="{{asset('assets/gacha/sound/bg.mp3')}}" type="audio/mpeg">
        Your browser does not support the audio tag.
    </audio>
    <audio id="open-sound">
        <source src="{{asset('assets/gacha/sound/open.mp3')}}" type="audio/mpeg">
        Your browser does not support the audio tag.
    </audio>
    <audio id="opens-sound">
        <source src="{{asset('assets/gacha/sound/opens.mp3')}}" type="audio/mpeg">
        Your browser does not support the audio tag.
    </audio>
    <audio id="opens2-sound">
        <source src="{{asset('assets/gacha/sound/opens2.mp3')}}" type="audio/mpeg">
        Your browser does not support the audio tag.
    </audio>
    <div id="error-message" style="display: none;">
        <!-- Pesan error akan ditampilkan di sini -->
    </div>
    <div class="curtain curtain-left"></div>
    <div class="curtain curtain-right"></div>
    <!-- partial:index.partial.html -->
    <canvas></canvas>

    <!-- <div class="headline">FILI GOT IT</div> -->

    <div class="instructions">
        <div class="neon-text">
            {{-- <p class="main-text">FILI GOT IT !</p>
            <p class="sub-text">by Axsis</p> --}}
            <img src="{{asset('assets/gacha/logo_copy.png')}}" class="logo" alt="">

        </div>

    </div>

    <div class="present">
        <img class="shine" src="{{asset('assets/gacha/shine.webp')}}" alt="the shine">
        <img class="gift" src="{{asset('assets/gacha/shine.webp')}}" alt="the gift">
        <!-- <div class="gift">Hektor</div> -->

        <div class="wiggle-container wiggle">
            <div class="rotate-container">
                <div class="bottom"></div>
                <div class="front"></div>
                <div class="left"></div>
                <div class="back"></div>
                <div class="right"></div>

                <div class="lid">
                    <div class="lid-top"></div>
                    <div class="lid-front"></div>
                    <div class="lid-left"></div>
                    <div class="lid-back"></div>
                    <div class="lid-right"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="errorModal" tabindex="-1" aria-labelledby="errorModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                    <p id="error-message-body"></p>
                    <a id="link-message-body"></a>
                </div>
            </div>
        </div>
    </div>
    <section>

        <div class="running-text">
            <marquee behavior="scroll" direction="left">
                {{getRunningTextMessages()}}
            </marquee>
        </div>
        <div class="h-container">
            <div class="left-c">
                {{-- <div class="box-l">
                    @if (getPrizeId(1,1))
                    <img src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(1,1)['gachamasterdetail_id']) ?>"
                        alt="Prize Image">
                    <br>
                    <p>
                        {{cekuser(getPrizeId(1,1)['user_id'])}}
                    </p>
                    @endif
                </div> --}}
                <div class="row">
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(1,1))
                            <div class="image-container">
                                <img class="img-fluid"
                                    src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(1,1)['gachamasterdetail_id']) ?>"
                                    alt="Prize Image">
                            </div>
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(1,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(2,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(2,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(2,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(3,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(3,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(3,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(4,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(4,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(4,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(5,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(5,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(5,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(6,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(6,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(6,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(7,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(7,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(7,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(8,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(8,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(8,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(9,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(9,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(9,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            @if (getPrizeId(10,1))
                            <img class="img-fluid"
                                src="<?php echo asset('assets/images/gacha/').'/'.cekprizeimage(getPrizeId(10,1)['gachamasterdetail_id']) ?>"
                                alt="Prize Image">
                            <p class="overlay-text">
                                {{cekuser(getPrizeId(10,1)['user_id'])}}
                            </p>
                            @else
                            <img src="{{asset('assets/gacha/new/white.png')}}" class="img-fluid" alt="Prize Image">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="bubble-container">
                <?php foreach ($messages as $index => $message): ?>
                <div class="bubble bubble-<?php echo $index; ?>">
                    <span class="bubble-text">
                        <?php echo htmlspecialchars($message, ENT_QUOTES, 'UTF-8'); ?>
                    </span>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="right-c">
                <div class="row">
                    <div class="col-12 mb-2">
                        <div class="card px-5 py-1">
                            <img class="img-fluid" src="{{asset('assets/gacha/2.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <div class="card px-4">
                            <img class="img-fluid" src="{{asset('assets/gacha/3.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <div class="card py-1">
                            <img class="img-fluid" src="{{asset('assets/gacha/1.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/6.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/7.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/imgg.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/IMG_28571.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Blitariancoffee.png')}}"
                                alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-1.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-2.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-3.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-4.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-5.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-6.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-7.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-8.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-9.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-10.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 2rem !important;padding-right: 2rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/Img-11.png')}}" alt="Prize Image">
                        </div>
                    </div>
                    <div class="col-6 mb-2">
                        <div class="card" style="padding-left: 1rem !important;padding-right: 1rem !important;">
                            <img class="img-fluid" src="{{asset('assets/gacha/new/image3.png')}}" alt="Prize Image">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <footer>
        <button class="open-again-btn" style="display: none;">Open Again</button>
    </footer>
    <!-- partial -->
    {{--
    <script src="./script.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script>
        window.addEventListener('load', function() {
        setTimeout(function() {
            document.querySelector('.curtain-left').classList.add('open-left');
            document.querySelector('.curtain-right').classList.add('open-right');
        }, 500); // Delay for effect
        
        const curtainSound = document.getElementById('curtain-sound');
        const bgSound = document.getElementById('bg-sound');

        // Menunggu hingga audio 'tirai.mp3' selesai diputar
        curtainSound.addEventListener('ended', function() {
            // Set timeout untuk memberi jeda 2 detik
            setTimeout(function() {
                // Mulai memainkan audio 'bg.mp3'
                bgSound.play();
            }, 1000); // Jeda 2 detik (2000 milidetik)
        });

        const playButton = document.getElementById('play-button');

        playButton.addEventListener('click', function() {
            if (bgSound.paused) {
                bgSound.play();
                playButton.innerHTML = '<i class="fa fa-pause"></i>';
            } else {
                bgSound.pause();
                playButton.innerHTML = '<i class="fa fa-play"></i>';
            }
        });



    });
    </script>


    <script>
        const countsNeeded = 1;
let counts = 1;
let isAnimating = false;

const prizes = [
    {
        image: "{{ asset('assets/gacha/3_BRO.png') }}",
        title: '3 BRO',
        id: 1
    },
    {
        image: "{{ asset('assets/gacha/1_BRO.png') }}",
        title: '1 BRO',
        id: 2
    },
    {
        image: "{{ asset('assets/gacha/vortis.png') }}",
        title: 'vortis',
        id: 3
    },
    {
        image: "{{ asset('assets/gacha/alhambra.png') }}",
        title: 'alhambra',
        id: 4
    },
    {
        image: "{{ asset('assets/gacha/athenia.png') }}",
        title: 'athenia',
        id: 5
    }
];

const present = document.querySelector('.present');
const giftImage = document.querySelector('.gift');
const giftTitle = document.querySelector('.gift-title');  // Assuming there's an element for the title

present.addEventListener('click', () => {
    if (!present.classList.contains('open') && !isAnimating) {
        counts += 1;
        present.style.setProperty('--count', Math.ceil(counts / 2));
        present.classList.add('animate');
        present.classList.add('animate2');
        // Memilih elemen audio
        const bgSound = document.getElementById('bg-sound');
        const opensSound = document.getElementById('opens-sound');
        const opens2Sound = document.getElementById('opens2-sound');

        // Perbarui tampilan $kuota


        opensSound.play();
        bgSound.pause();

        // Menambahkan event listener untuk mengatur ulang audio latar belakang setelah 'open.mp3' selesai diputar
        opensSound.addEventListener('ended', function() {
                // Set timeout untuk memberi jeda 2 detik
                setTimeout(function() {
                    // Mulai memainkan audio 'bg.mp3'
                    bgSound.play();
                }, 1000); // Jeda 2 detik (2000 milidetik)
        });
        isAnimating = true;

        setTimeout(() => {
            present.classList.remove('animate');
        }, 300);

        setTimeout(() => {
            present.classList.remove('animate2');
            isAnimating = false;
        }, 2000);

        setTimeout(() => {
            if (counts >= countsNeeded) {
                fetch("{{ route('gacha.store',['id' => $userid]) }}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                })
                .then(response => {
                    if (!response.ok) {
                        return response.json().then(errorData => { 
                            throw new Error(errorData.error || 'Unknown error');
                        });
                    }
                    return response.json();
                })
                .then(data => {
                    // console.log('Success:', data);
                    if (data.message && data.item) {
                            opens2Sound.play();
                            
                            updateKuota();

                            giftImage.src = "{{ asset('assets/images/gacha/') }}/" +data.item.image;
                            giftImage.alt = data.item.name;
                            setTimeout(() => {
                                document.querySelector('.open-again-btn').style.display = 'block';
                            }, 2000);

                    }

                    present.classList.add('open');
                })
                .catch(error => {
                    const errorMessageBody = document.getElementById('error-message-body');
                    const linkMessageBody = document.getElementById('link-message-body');
                    if (errorMessageBody) {
                        errorMessageBody.textContent = error.message;
                        linkMessageBody.textContent = error.links;
                        const errorModal = new bootstrap.Modal(document.getElementById('errorModal'));
                        errorModal.show();
                    } else {
                        // Fallback jika elemen tidak ditemukan
                        alert('Error: ' + error.message);
                    }
                });

            }
        }, 2000);
    }
});

function updateKuota() {
    // Perbarui teks $kuota di elemen yang sesuai
    const kuotaSpan = document.getElementById('kuota');
    if (kuotaSpan) {
            const kuotaValue = kuotaSpan.textContent.trim(); // Mengambil teks dan membuang spasi ekstra
            const kuotaNumber = parseInt(kuotaValue); // Mengubah teks menjadi angka (jika perlu)
            kuotaSpan.textContent = kuotaNumber - 1;
    }

}


        const canvas = document.querySelector('canvas');
        const ctx = canvas.getContext('2d');

        let width;
        let height;
        let lastNow;
        let snowflakes;
        let maxSnowflakes = 50; // Reduced max number of stars

        const rand = (min, max) => min + Math.random() * (max - min);

        class Snowflake {
            constructor() {
                this.spawn(true);
            }

            spawn(anyY = false) {
                this.x = rand(0, width);
                this.y = anyY === true
                    ? rand(-50, height + 50)
                    : rand(-50, -10);
                this.xVel = rand(-0.05, 0.05);
                this.yVel = rand(0.02, 0.1);
                this.angle = rand(0, Math.PI * 2);
                this.angleVel = rand(-0.001, 0.001);
                this.size = rand(2, 7); // Smaller size
                this.sizeOsc = rand(0.01, 0.5);
            }

            update(elapsed) {
                const xForce = rand(-0.001, 0.001);

                if (Math.abs(this.xVel + xForce) < 0.075) {
                    this.xVel += xForce;
                }

                this.x += this.xVel * elapsed;
                this.y += this.yVel * elapsed;
                this.angle += this.xVel * 0.05 * elapsed;

                if (
                    this.y - this.size > height
                    || this.x + this.size < 0
                    || this.x - this.size > width
                ) {
                    this.spawn();
                }

                this.render();
            }

            render() {
                ctx.save();
                const { x, y, size, angle } = this;
                ctx.translate(x, y);
                ctx.rotate(angle);

                // Draw a star outline
                ctx.beginPath();
                for (let i = 0; i < 5; i++) {
                    ctx.lineTo(0, size);
                    ctx.translate(0, size);
                    ctx.rotate((Math.PI * 2 / 10));
                    ctx.lineTo(0, -size);
                    ctx.translate(0, -size);
                    ctx.rotate(-(Math.PI * 6 / 10));
                }
                ctx.closePath();
                ctx.strokeStyle = 'yellow'; // Yellow color
                ctx.shadowColor = 'yellow'; // Yellow shadow
                ctx.shadowBlur = 10;
                ctx.lineWidth = 2;
                ctx.stroke();
                ctx.restore();
            }
        }

        function render(now) {
            requestAnimationFrame(render);

            const elapsed = now - lastNow;
            lastNow = now;

            ctx.clearRect(0, 0, width, height);
            if (snowflakes.length < maxSnowflakes) {
                snowflakes.push(new Snowflake());
            }

            ctx.strokeStyle = 'rgba(255, 255, 255, .5)';
            ctx.fillStyle = 'rgba(255, 255, 255, .5)';

            snowflakes.forEach((snowflake) => snowflake.update(elapsed, now));
        }

        function resize() {
            width = window.innerWidth;
            height = window.innerHeight;

            canvas.width = width;
            canvas.height = height;

            maxSnowflakes = Math.max(width / 20, 50); // Reduced number of stars based on width
        }

        function pause() {
            cancelAnimationFrame(render);
        }
        function resume() {
            lastNow = performance.now();
            requestAnimationFrame(render);
        }

        function init() {
            snowflakes = [];
            resize();
            render(lastNow = performance.now());
        }

        window.addEventListener('resize', resize);
        window.addEventListener('blur', pause);
        window.addEventListener('focus', resume);

        init();

    </script>
    <script>
        const openAgainBtn = document.querySelector('.open-again-btn');

    openAgainBtn.addEventListener('click', () => {
        // Reset counts
        counts = 1;
        present.style.setProperty('--count', Math.ceil(counts / 2));

        // Reset visual effects (classes and animations)
        // present.classList.remove('animate2');
        // present.classList.remove('animate');
        present.classList.remove('open');

        // Reset gift image and title
        giftImage.src = "{{ asset('assets/gacha/shine.webp') }}";
        giftImage.alt = "the gift";

        // Reset any other necessary states or elements
        setTimeout(() => {
                                document.querySelector('.open-again-btn').style.display = 'none';
                            }, 1000);
    });

    </script>
</body>

</html>