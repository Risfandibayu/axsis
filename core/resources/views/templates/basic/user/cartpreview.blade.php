@extends($activeTemplate.'layouts.master')
@section('content')

<div class="container">
    <div class="row  justify-content-center">
        <div class="col-md-12 col-lg-12 col-xl-12 col-xxl-12">
            <div class="card card-deposit text-center">
                <div class="card-body card-body-deposit card-body">
                    <div class="">
                        {{-- <div class="deposit-thumb">
                            <img class="" src="{{ $data->gateway_currency()->methodImage() }}" />
                        </div> --}}
                        <div class="deposit-content">
                            <ul class="mb-3">
                                <li>
                                    <table class="table table--light style--two">
                                        <thead>
                                            <tr>
                                                {{-- <th scope="col">@lang('#')</th> --}}
                                                <th scope="col">@lang('Product')</th>
                                                <th scope="col" width="20%">@lang('Qty')</th>
                                                <th scope="col" width="20%">@lang('Total')</th>
                                                {{-- <th scope="col" width="20%">@lang('Weight')</th> --}}

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($cartlist as $ex)
                                            <?php  $price = (firstrx2(Auth::user()->id)) ? $ex->product->price_public : $ex->product->price;?>
                                            <tr>
                                                {{-- <td data-label="@lang('#')">{{ $devcart->firstItem()+$loop->index
                                                    }}</td> --}}
                                                <td data-label="@lang('Product')">
                                                    <div class="row d-flex">
                                                        <div class="book">
                                                            <img src="{{ getImage('assets/images/products/'. $ex->product->thumbnail,  null, true)}}"
                                                                class="book-img">
                                                        </div>
                                                        <div class="my-auto flex-column d-flex pad-left">
                                                            <h6 class="mob-text text-left font-weight-bold ml-2">Product
                                                                Name : {{
                                                                $ex->product->name }} </h6>
                                                            {{-- <h6 class="mob-text text-left font-weight-bold ml-2">
                                                                Weight : {{
                                                                $ex->product->weight }} gr</h6> --}}
                                                        </div>
                                                    </div>


                                                </td>
                                                <td data-label="@lang('Qty')">
                                                    {{-- {{ $ex->qty }} --}}
                                                    <div class="input-group">

                                                        <input type="text" readonly class="form-control input-number"
                                                            value="{{ $ex->qty }} pcs" min="1"
                                                            max="{{$ex->product->quantity}}">

                                                    </div>
                                                </td>
                                                <td data-label="@lang('Total')">
                                                    {{-- {{ $ex->qty }} --}}
                                                    <div class="input-group">

                                                        <input type="text" readonly class="form-control input-number"
                                                            value="Rp. {{ nb($price * $ex->qty) }}" min="1"
                                                            max="{{$ex->product->quantity}}">

                                                    </div>
                                                </td>
                                                {{-- <td data-label="@lang('Weight')">
                                                    <div class="input-group">

                                                        <input type="text" readonly class="form-control input-number"
                                                            value="{{ $ex->qty * $ex->gold->prod->weight }} gr" min="1"
                                                            max="{{$ex->gold->qty}}">

                                                    </div>
                                                </td> --}}


                                            </tr>
                                            @empty
                                            <tr>
                                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message)
                                                    }}</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                </li>
                                {{-- <li>
                                    @lang('Total Weight'):
                                    <b class="fw-bolder"><span class="">{{$berattotal}} Gr</b>
                                </li> --}}
                                <li>
                                    @lang('Total Pieces'):
                                    <b class="fw-bolder"><span class="">{{$keptotal}} Pcs</b>
                                </li>
                                <li>
                                    @lang('Total'):
                                    <b class="fw-bolder"><span class="">Rp. {{nb($total)}}</b>
                                </li>
                                <hr>
                                <li>
                                    @lang('Recipient`s name'):
                                    <b class="fw-bolder"><span class="">{{$alamat->nama_penerima}}</b>
                                </li>
                                <li>
                                    @lang('Recipient`s phone number'):
                                    <b class="fw-bolder"><span class="">{{$alamat->no_telp}}</b>
                                </li>
                                <li>
                                    @lang('Full Address'):
                                    <b class="fw-bolder"><span class="">{{ Str::limit($alamat->alamat,40) }}</b>
                                </li>
                                <li>
                                    @lang('Postal Code'):
                                    <b class="fw-bolder"><span class="">{{$alamat->kode_pos}}</b>
                                </li>
                                <hr>
                                <li>
                                    @lang('Shipping Cost'):
                                    <b class="fw-bolder"><span class="">Rp. {{nb($ongkir)}}</b>
                                </li>
                                <li>
                                    @lang('Grand Total'):
                                    <b class="fw-bolder"><span class="">Rp. {{nb($ongkir + $total)}}</b>
                                </li>

                            </ul>
                            <hr>
                            <div class="card-body mb-3 row justify-content-end">
                                <div class="col-md-6">

                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Select Payment Method</h5>
                                        <form action="{{route('user.checkoutconfirm')}}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="ongkir" value="{{$ongkir}}">
                                            <input type="hidden" name="alamat" value="{{$alamat->id}}">
                                            <input type="hidden" name="total" value="{{$ongkir + $total}}">
                                            <div   div class="form-group">
                                                    <select name="payment" id="payment" class="form-control form-control-lg" required>
                                                        <option value="" hidden selected>-- Select Payment Method --</option>
                                                        <option @if (Auth::user()->balance < ($ongkir + $total))
                                                            disabled
                                                        @endif value="balance">Balance ({{nb(Auth::user()->balance)}})</option>
                                                        <option value="other">Other Payment</option>
                                                </select>
                                            </div>

                                            <button type="submit" class="btn btn-lg btn--info" style="width: 100%;"
                                                onclick="this.form.submit(); this.disabled=true; this.value='Sending…';">@lang('Confirm')</button>
                                        </form>

                                    </div>
                                    </div>
                                </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection