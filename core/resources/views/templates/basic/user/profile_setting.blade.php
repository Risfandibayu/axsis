@extends($activeTemplate.'layouts.master')
@section('content')
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link text-dark" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="true">@lang('Profile')</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link text-dark" id="edit-profile-tab" data-bs-toggle="tab" data-bs-target="#edit-profile" type="button" role="tab" aria-controls="edit-profile" aria-selected="false">edit-profile</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link text-dark" id="alamat-tab" data-bs-toggle="tab" data-bs-target="#alamat" type="button" role="tab" aria-controls="alamat" aria-selected="false">Shipping
        Address</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link text-dark" id="bank-tab" data-bs-toggle="tab" data-bs-target="#bank" type="button" role="tab" aria-controls="bank" aria-selected="false">bank</button>
    </li>

  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card overflow-hidden shadow mt-5 border-0 rounded">
            <div class="card-body p-0 ">
                <div class="p-3 bg--white">
                    <div class="dashboard-user">
                    <div class="user-thumb">
                        <img src="{{ getImage('assets/images/user/profile/'. auth()->user()->image, '350x300')}}" alt="dashboard">
                    </div>
                </div>


                    <ul class="list-group mt-3">
                        <li class="list-group-item d-flex justify-content-between">
                            <span>@lang('Name')</span> {{auth()->user()->fullname}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('Username')</span> {{auth()->user()->username}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('Email')</span> {{auth()->user()->email}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('Mobile Number')</span> {{auth()->user()->mobile}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('Address')</span> {{auth()->user()->address->address}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('City')</span> {{auth()->user()->address->city}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('State')</span> {{auth()->user()->address->state}}
                        </li>
                        <li class="list-group-item rounded-0 d-flex justify-content-between">
                            <span>@lang('Zip/Postal')</span> {{auth()->user()->address->zip}}
                        </li>

                        <li class="list-group-item d-flex justify-content-between">
                            <span>@lang('Joined at')</span> {{date('d M, Y h:i A',strtotime(auth()->user()->created_at))}}
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="edit-profile" role="tabpanel" aria-labelledby="edit-profile-tab">
        <div class="card overflow-hidden shadow mt-5 border-0 rounded">
            <div class="card-body">
                <h5 class="card-title mb-50 border-bottom pb-2">{{auth()->user()->fullname}} @lang('Information')</h5>

                <form class="register prevent-double-click" action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="InputFirstname" class="col-form-label">@lang('First Name'):</label>
                            <input type="text" class="form-control " id="InputFirstname" name="firstname" placeholder="@lang('First Name')" value="{{$user->firstname}}" minlength="3">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="lastname" class="col-form-label">@lang('Last Name'):</label>
                            <input type="text" class="form-control " id="lastname" name="lastname" placeholder="@lang('Last Name')" value="{{$user->lastname}}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="email" class="col-form-label">@lang('E-mail Address'):</label>
                            <input class="form-control " id="email" placeholder="@lang('E-mail Address')" value="{{$user->email}}" readonly>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="phone" class="col-form-label">@lang('Mobile Number')</label>
                            <input class="form-control " id="phone" value="{{$user->mobile}}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="address" class="col-form-label">@lang('Address'):</label>
                            <input type="text" class="form-control " id="address" name="address" placeholder="@lang('Address')" value="{{@$user->address->address}}" required="">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="state" class="col-form-label">@lang('State'):</label>
                            <br>
                            {{-- <input type="text" class="form-control " id="state" name="state" placeholder="@lang('state')" value="{{@$user->address->state}}" required=""> --}}
                            <select id="single2" name="state" class="js-states form-control ">
                                {{-- @foreach ($kota as $item)
                                    <option value="">{{$item->city}}</option>
                                @endforeach --}}
                                <option value="{{$user->address->state}}">{{$user->address->state}}</option>
                             </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="zip" class="col-form-label">@lang('Zip Code'):</label>
                            <input type="text" class="form-control " id="zip" name="zip" placeholder="@lang('Zip Code')" value="{{@$user->address->zip}}" required="">
                        </div>

                        <div class="form-group col-sm-4">
                            <label for="city" class="col-form-label">@lang('City'):</label>
                            <br>
                            {{-- <input type="text" class="form-control " id="city" name="city" placeholder="@lang('City')" value="{{@$user->address->city}}" required=""> --}}
                            <select id="single" name="city" class="js-states form-control ">
                                {{-- @foreach ($kota as $item)
                                    <option value="">{{$item->city}}</option>
                                @endforeach --}}
                                <option value="{{$user->address->city}}">{{$user->address->city}}</option>
                             </select>
                        </div>

                        <div class="form-group col-sm-4">
                            <label class="col-form-label">@lang('Country'):</label>
                            <input class="form-control " value="{{@$user->address->country}}" disabled>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <img src="{{ getImage(imagePath()['profile']['user']['path'].'/'. $user->image,imagePath()['profile']['user']['size']) }}" alt="@lang('Image')">
                            <div class="form-group">
                                <label>@lang('Image')</label>
                                <input class="form-control" type="file" name="image">
                                <code>@lang('Image size') {{imagePath()['profile']['user']['size']}}</code>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row pt-5">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn--base w-100">@lang('Update Profile')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="alamat" role="tabpanel" aria-labelledby="alamat-tab">
        <div class="card overflow-hidden shadow mt-5 border-0 rounded">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <h5 class="card-title mb-50 border-bottom pb-2">{{auth()->user()->fullname}} @lang('Shipping
                            Address Information')</h5>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-success add-address">Add New Address</button>
                    </div>
                </div>
                <div class="table-responsive--sm table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('#')</th>
                                <th scope="col">@lang('Recipient`s name')</th>
                                <th scope="col">@lang('Recipient`s phone number')</th>
                                <th scope="col">@lang('Full Address')</th>
                                <th scope="col">@lang('Postal Code')</th>
                                <th scope="col">@lang('Default Address')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0; ?>
                            @foreach ($alamat as $item)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$item->nama_penerima}}</td>
                                <td>{{$item->no_telp}}</td>
                                <td>{{$item->alamat}}</td>
                                <td>{{$item->kode_pos}}</td>
                                <td>@if($item->selected == 1)
                                    <span class="badge custom--badge">Active</span>
                                @endif</td>
                                <td data-label="@lang('Action')">
                                    <?php 
                                        $array = explode(', Kec. ', $item->alamat??'');
                                        $array2 = explode(', Kab/Kota ', $array[1]??'');
                                        $array3 = explode(', Prov. ', $array2[1]??'');
                                    ?>
                                    <button type="button" class="editaddress btn btn-warning btn-sm" data-toggle="tooltip"
                                        data-id="{{ $item->id }}" data-alamat="{{ $array[0]??'' }}"
                                        data-nama_penerima="{{ $item->nama_penerima }}"
                                        data-no_telp="{{ $item->no_telp }}" data-kode_pos="{{ $item->kode_pos }}"
                                        data-kec="{{ $array2[0]??'' }}" data-kab="{{ $array3[0]??'' }}"
                                        data-prov="{{ $array3[1]??'' }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i> Edit
                                    </button>
                                    <form method="post" action="{{route('user.set_default')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <button type="sbumit" class="mt-2 btn btn-success btn-sm">
                                            <i class="las la-check-circle"></i> Set Default
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table><!-- table end -->
                </div>


            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
        <div class="card overflow-hidden shadow mt-5 border-0 rounded">
            <div class="card-body">
                <h5 class="card-title mb-50 border-bottom pb-2">{{auth()->user()->fullname}} @lang('Bank Account
                    Information')</h5>
                @if ($bank_user)
                {{-- <form action="{{route('user.add_rekening')}}" method="POST" enctype="multipart/form-data"
                    id="edit">
                    @csrf --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="form-control-label font-weight-bold">@lang('Bank Name') <span
                                        class="text-danger">*</span></label>
                                {{-- <input class="form-control form-control-lg" type="text" name="firstname"
                                    value="{{auth()->user()->firstname}}" required> --}}
                                <select name="bank_name" id="bank_name" class="form-control " disabled>
                                    @foreach ($bank as $item)
                                    <option value="{{$item->code}}" {{auth()->user()->userBank->nama_bank ==
                                        $item->code ? 'selected' : '';}}>{{$item->nama_bank}}

                                        {{-- <input type="hidden" name="bank" value="{{$item->name}}"> --}}
                                    </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Bank Branch City')
                                    <small>(Optional)</small></label>
                                <input class="form-control " type="text" name="kota_cabang"
                                    value="{{auth()->user()->userBank->kota_cabang}}" placeholder="Bank KCP Jakarta"
                                    readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Bank Account Name') <span
                                        class="text-danger">*</span></label>
                                <input class="form-control " type="text" name="acc_name"
                                    value="{{auth()->user()->userBank->nama_akun}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Bank Account Number') <span
                                        class="text-danger">*</span></label>
                                <input class="form-control " type="text" name="acc_number"
                                    value="{{auth()->user()->userBank->no_rek}}" readonly>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn--primary btn-block btn-lg">@lang('Save
                                    Changes')</button>
                            </div>
                        </div>
                    </div>
                </form> --}}
                @else
                <form action="{{route('user.add_rekening')}}" method="POST" enctype="multipart/form-data" id="add">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="form-control-label font-weight-bold">@lang('Bank Name') <span
                                        class="text-danger">*</span></label>
                                {{-- <input class="form-control form-control-lg" type="text" name="firstname"
                                    value="{{auth()->user()->firstname}}" required> --}}
                                <select name="bank_name" id="bank_name" class="form-control " required>
                                    <option value="" hidden selected>-- Pilih Bank --</option>
                                    @foreach ($bank as $item)
                                    <option value="{{$item->code}}">{{$item->nama_bank}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Bank Branch City')
                                    <span class="text-danger">*</span></label>
                                <input class="form-control " type="text" name="kota_cabang" value=""
                                    placeholder="Bank KCP Jakarta" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Account Name') <span
                                        class="text-danger">*</span></label>
                                <input class="form-control " type="text" name="acc_name" value=""
                                    required placeholder="Account Name">
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="form-control-label  font-weight-bold">@lang('Account Number') <span
                                        class="text-danger">*</span></label>
                                <input class="form-control " type="text" placeholder="Account Number"
                                    name="acc_number" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn--base w-100">@lang('Save
                                    Changes')</button>
                            </div>
                        </div>
                    </div>
                </form>
                @endif


            </div>
        </div>
    </div>
    
  </div>

  @push('modal')
      
  <div id="add-address" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Address')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('user.add_address')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s name')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control" name="nama_penerima" placeholder="Nama Penerima"
                            required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s phone number')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control" name="no_telp" placeholder="Nomor Telepon Penerima"
                            required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Address')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <textarea style="border:inline" class="form-control" id="alamat" name="alamat" rows="4" placeholder="Jalan, No Rumah, RT/RW, Desa/Dusun"
                            required></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Districts')</label>
                            <input type="text" class="form-control" name="kec" placeholder="Kecamatan" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('City')</label>
                            <input type="text" class="form-control" name="kab" placeholder="Kota/Kabupaten" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Province')</label>
                            <input type="text" class="form-control" name="prov" placeholder="Provinsi" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Postal Code')</label>
                            {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                                onchange="loadFile(event)" name="images" required> --}}
                            <input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos Penerima"
                                required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="edit-address" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Address')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('user.edit_address')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="hidden" class="id" name="id" id="id">
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Address')</label>
                            <Textarea class="alamat" id="alamat" name="alamat" rows="4"
                                placeholder="Jalan, No Rumah, RT, RW , Kec/Kota, Kab, No Pos"></Textarea>
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s name')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control nama_penerima" id="nama_penerima" name="nama_penerima"
                            placeholder="Nama Penerima" required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s phone number')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control no_telp" id="no_telp" name="no_telp"
                            placeholder="Nomor Telepon Penerima" required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Address')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <textarea style="border:inline" class="alamat form-control" id="alamat" name="alamat" rows="4"
                            placeholder="Jalan, No Rumah, RT, RW , Kec/Kota, Kab" required></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Districts')</label>
                            <input type="text" class="form-control kec" name="kec" placeholder="Kecamatan" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('City')</label>
                            <input type="text" class="form-control kab" name="kab" placeholder="Kota/Kabupaten"
                                required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Province')</label>
                            <input type="text" class="form-control prov" name="prov" placeholder="Provinsi" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Postal Code')</label>
                            {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                                onchange="loadFile(event)" name="images" required> --}}
                            <input type="text" class="form-control kode_pos" name="kode_pos"
                                placeholder="Kode Pos Penerima" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endpush

@endsection


@push('script')
<script>
    'use strict';
        (function($){
            $("select[name=country]").val("{{ auth()->user()->address->country }}");
        })(jQuery)

        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

        $('.add-address').on('click', function () {
                var modal = $('#add-address');
                modal.modal('show');
        });
        $('.editaddress').on('click', function () {
            // console.log($(this).data('alamat'));
                var modal = $('#edit-address');
                modal.find('.id').val($(this).data('id'));
                modal.find('.alamat').val($(this).data('alamat'));
                modal.find('.nama_penerima').val($(this).data('nama_penerima'));
                modal.find('.no_telp').val($(this).data('no_telp'));
                modal.find('.kode_pos').val($(this).data('kode_pos'));
                modal.find('.kec').val($(this).data('kec'));
                modal.find('.kab').val($(this).data('kab'));
                modal.find('.prov').val($(this).data('prov'));
                modal.modal('show');
        });

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#single").select2({
        placeholder: "Example: Sleman",
                closeOnSelect: false,
                allowClear: true,
                delay: 250, // wait 250 milliseconds before triggering the request
                ajax: {
                    url: "{{ route('city') }}",
                    dataType: "json",
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        var results = [];
                        $.each(data, function(index, item) {
                            results.push({
                                id: item.city,
                                text: item.city,
                                value: item.city
                            })
                        })
                        return {
                            results: results
                        };
                    }
                }
      });
</script>
<script>
    $("#single2").select2({
        placeholder: "Example: DKI Jakarta",
                closeOnSelect: false,
                allowClear: true,
                delay: 250, // wait 250 milliseconds before triggering the request
                ajax: {
                    url: "{{ route('province') }}",
                    dataType: "json",
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        var results = [];
                        $.each(data, function(index, item) {
                            results.push({
                                id: item.province,
                                text: item.province,
                                value: item.province
                            })
                        })
                        return {
                            results: results
                        };
                    }
                }
      });
</script>
<script>
$(document).ready(function(){
    var hash = window.location.hash;
    if (hash) {
        console.log(hash);
        $(hash).addClass('tab-pane fade active show');
        var targetElement = document.querySelector('[data-bs-target="' + hash + '"]');
        if (targetElement) {
            targetElement.classList.add('nav-link', 'active', 'text-dark');
        }
    }
});
</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
      const tabs = document.querySelectorAll('.nav-link');
      tabs.forEach(tab => {
        tab.addEventListener('click', function() {
          const targetId = this.getAttribute('data-bs-target');
          window.location.hash = targetId;
        });
      });
  
      // Check if there's a hash in the URL on page load
      if (window.location.hash) {
        const hash = window.location.hash.substring(1); // Remove the '#'
        const targetTab = document.querySelector(`[data-bs-target="${hash}"]`);
        if (targetTab) {
          targetTab.click(); // Simulate a click on the corresponding tab
        }
      }
    });
  </script>
@endpush