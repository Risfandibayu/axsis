<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Saka Vaganza</title>
    <link rel="shortcut icon" type="image/png" href="{{getImage(imagePath()['logoIcon']['path'] .'/favicon.png')}}">
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            background: rgb(190,247,255);
            background: linear-gradient(133deg, rgba(190,247,255,1) 25%, rgba(255,212,70,1) 79%);
            min-height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            background-size: 400% 400%;
            animation: gradientAnimation 10s ease infinite;
        }

        @keyframes gradientAnimation {
            0% { background-position: 0% 50%; }
            50% { background-position: 100% 50%; }
            100% { background-position: 0% 50%; }
        }

        .container {
            background: rgba(255, 255, 255, 0);
            padding: 20px;
            border-radius: 10px;
            max-width: 5140px;
        }

        .coin {
            position: absolute;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background: linear-gradient(135deg, #ffd700 25%, #f5c242 50%, #ffd700 75%);
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
            transform: rotate(45deg);
        }
        #tsparticles {
            position: fixed; /* atau absolute, sesuai kebutuhan */
            z-index: -1;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }
        #tsparticles2 {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9998;
            pointer-events: none;
        }
        .coins {
        position: absolute;
            top: 30px;
            left: 30px;
            background-color: white;
            padding: 5px 20px;
            font-family: 'Barlow Condensed', sans-serif;
            font-size: 32px;
            font-weight: bold;
            border-radius: 20px;
            z-index: 1;
        }
        .play {
        position: absolute;
            top: 30px;
            left: 270px;
            background-color: white;
            padding: 5px 20px;
            font-family: 'Barlow Condensed', sans-serif;
            font-size: 32px;
            font-weight: bold;
            border-radius: 30px;
            z-index: 1;
        }
        .spasi{
                
          margin-top:-50px !important;
        }
        .logo{
          max-width: 100%;
          height: 300px;
        }
        @media screen and (max-width: 768px){
            .spasi{
                margin-top:50px !important;
            }
            .logo{
              max-width: 100%;
              height: 200px;
            }
        }
        .image-container {
    height: 150px; /* Adjust this value as needed */
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
}

.image-container img {
    max-height: 100%;
    width: auto;
}
    </style>
</head>
<body>
    <audio id="bg-sound" loop>
      <source src="{{asset('assets/gacha/sound/DuckDance.mp3')}}" type="audio/mpeg">
      Your browser does not support the audio tag.
  </audio>
    <audio id="tada-sound">
      <source src="{{asset('assets/gacha/sound/tada.mp3')}}" type="audio/mpeg">
      Your browser does not support the audio tag.
  </audio>
    <div class="coins">
        <i class="fa fa-ticket"></i> <span id="kuota">{{$kuota}}</span> Tiket
    </div>
    <div class="play" id="play-button">
      <i class="fa fa-play"></i>
    </div>
    <div id="particles-js" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: -1;"></div>

    <div class="container spasi">
        {{-- <h1 class="text-center mb-4">Saka Vaganza by Axsis</h1> --}}
        <div style="text-align: center;">
          <img src="{{asset('assets/gacha/logo_saka_vaganza.png')}}" alt="" class="logo">
        </div>
        <div class="row">
            <!-- Product Card 1 -->
            @foreach ($items as $item)
            <div class="col-6 col-md-2 mb-4">
              <div class="card shadow-lg">
                  <div class="image-container">
                      <img src="{{ getImage('assets/images/redeem/'. $item->image,  null, true)}}" alt="Image {{$item->name}}" class="card-img-top" alt="Product 1">
                  </div>
                  <div class="card-body">
                      <h5 class="card-title">{{$item->name}}</h5>
                      @if ($item->qty>0)
                      <p class="card-text">Sisa Hadiah : {{$item->qty}}
                        <br>
                        Stock: <span class="text-success">Available</span></p>
                      @else
                      <p class="card-text">Sisa Hadiah : {{$item->qty}}
                        <br>
                        Stock: <span class="text-danger">Unavailable</span></p>
                      @endif
                      <p class="card-text">Senilai Rp. {{nb($item->modal)}}</p>
                      <button class="btn btn-primary btn-block claimButton" data-item="{{$item->id}}"><strong>FREE CLAIM</strong></button>
                  </div>
              </div>
          </div>
          
            @endforeach
            <!-- Additional Product Cards can be added here in similar fashion -->
        </div>
    </div>

    <!-- Bootstrap JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- tsParticles -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/tsparticles@3.0.2/tsparticles.bundle.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/tsparticles@1.30.3/tsparticles.min.js"></script> --}}

    <script>
        $(document).ready(function() {
          const bgSound = document.getElementById('bg-sound');
          bgSound.play();

          const playButton = document.getElementById('play-button');

          playButton.addEventListener('click', function() {
              if (bgSound.paused) {
                  bgSound.play();
                  playButton.innerHTML = '<i class="fa fa-pause"></i>';
              } else {
                  bgSound.pause();
                  playButton.innerHTML = '<i class="fa fa-play"></i>';
              }
          });

            $('.claimButton').click(function() {
                var itemId = $(this).data('item');
                
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('user.saka.vaganza.claim')}}", // Ganti dengan URL yang akan dihit
                    type: 'POST',
                    data: { item: itemId },
                    success: function(response) {
                        if (response.status === 200) {
                          var tadaSound = document.getElementById('tada-sound');
                          tadaSound.play();

                            Swal.fire({
                                text: response.message,
                                title: response.item.name,
                                imageUrl: "{{ asset('assets/images/redeem/') }}/"+response.item.image , // Ganti dengan path gambar yang benar
                                imageWidth: 200,
                                imageHeight: 200,
                                imageAlt: response.item.name,
                                willOpen: () => {
                    // Create a div for tsparticles
                                  const container = document.createElement('div');
                                  container.id = 'tsparticles2';
                                  document.body.appendChild(container);

                                  // Initialize tsparticles
                                  tsParticles.load({
                                    id: "tsparticles2",
                                    options: {
                                  fullScreen: {
                                    enable: true
                                  },
                                  particles: {
                                    number: {
                                      value: 0
                                    },
                                    color: {
                                      value: ["#1E00FF", "#FF0061", "#E1FF00", "#00FF9E"]
                                    },
                                    shape: {
                                      type: ["circle", "square"]
                                    },
                                    opacity: {
                                      value: {
                                        max: 1,
                                        min: 0
                                      },
                                      animation: {
                                        enable: true,
                                        speed: 2,
                                        startValue: "max",
                                        destroy: "min"
                                      }
                                    },
                                    size: {
                                      value: { min: 3, max: 7 }
                                    },
                                    life: {
                                      duration: {
                                        sync: true,
                                        value: 5
                                      },
                                      count: 1
                                    },
                                    move: {
                                      enable: true,
                                      gravity: {
                                        enable: true,
                                        acceleration: 20
                                      },
                                      speed: {
                                        min: 25,
                                        max: 50
                                      },
                                      drift: {
                                        min: -2,
                                        max: 2
                                      },
                                      decay: 0.05,
                                      direction: "none",
                                      outModes: {
                                        default: "destroy",
                                        top: "none"
                                      }
                                    },
                                    rotate: {
                                      value: {
                                        min: 0,
                                        max: 360
                                      },
                                      direction: "random",
                                      move: true,
                                      animation: {
                                        enable: true,
                                        speed: 60
                                      }
                                    },
                                    tilt: {
                                      direction: "random",
                                      enable: true,
                                      move: true,
                                      value: {
                                        min: 0,
                                        max: 360
                                      },
                                      animation: {
                                        enable: true,
                                        speed: 60
                                      }
                                    },
                                    roll: {
                                      darken: {
                                        enable: true,
                                        value: 25
                                      },
                                      enable: true,
                                      speed: {
                                        min: 15,
                                        max: 25
                                      }
                                    },
                                    wobble: {
                                      distance: 30,
                                      enable: true,
                                      move: true,
                                      speed: {
                                        min: -15,
                                        max: 15
                                      }
                                    }
                                  },
                                  interactivity: {
                                    detectsOn: "canvas",
                                    events: {
                                      resize: true,
                                      onClick: {
                                          enable: true,
                                          mode: "repulse"
                                      }
                                    }
                                  },
                                  detectRetina: true,
                                  responsive: [
                                    {
                                      maxWidth: 700,
                                      options: {
                                        particles: {
                                          move: {
                                            speed: 20,
                                            decay: 0.1
                                          }
                                        },
                                        emitters: [
                                          {
                                            direction: "top-right",
                                            rate: {
                                              delay: 0.1,
                                              quantity: 30
                                            },
                                            position: {
                                              x: 0,
                                              y: 20
                                            },
                                            size: {
                                              width: 0,
                                              height: 0
                                            }
                                          },
                                          {
                                            direction: "top-left",
                                            rate: {
                                              delay: 0.1,
                                              quantity: 30
                                            },
                                            position: {
                                              x: 100,
                                              y: 20
                                            },
                                            size: {
                                              width: 0,
                                              height: 0
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  ],
                                  emitters: [
                                    {
                                      direction: "top-right",
                                      rate: {
                                        delay: 0.1,
                                        quantity: 50
                                      },
                                      position: {
                                        x: 0,
                                        y: 50
                                      },
                                      size: {
                                        width: 0,
                                        height: 0
                                      }
                                    },
                                    {
                                      direction: "top-left",
                                      rate: {
                                        delay: 0.1,
                                        quantity: 50
                                      },
                                      position: {
                                        x: 100,
                                        y: 50
                                      },
                                      size: {
                                        width: 0,
                                        height: 0
                                      }
                                    }
                                  ]
                                    }
                                });
                                container.addEventListener('click', () => {
                                    Swal.close();
                                });

                              },
                              willClose: () => {
                                  const container = document.getElementById('tsparticles2');
                                  if (container) {
                                      container.remove();
                                  }
                              },
                              showConfirmButton: true,
                              confirmButtonText: 'YEAYY!!'
                            }).then((result) => {
                                if (result.isConfirmed || result.isDismissed) {
                                    // Reload the page after the alert is closed
                                    location.reload();
                                }
                            });
                            

                        } else {
                            Swal.fire({
                                title: "Error",
                                text: response.message,
                                icon: "error"
                            }).then((result) => {
                                if (result.isConfirmed || result.isDismissed) {
                                    // Reload the page after the alert is closed
                                    location.reload();
                                }
                            });
                        }
                        // console.log(response);
                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: "Something went wrong!"
                        }).then((result) => {
                            if (result.isConfirmed || result.isDismissed) {
                                // Reload the page after the alert is closed
                                location.reload();
                            }
                        });
                    }
                });
            });
        });
    </script>

    <script>
    (async () => {
  await tsParticles.load({
    id: "tsparticles",
    options: {

      particles: {
        shape: {
          type: "emoji",
          options: {
            emoji: {
              value: "💵"
            }
          }
        },
        life: {
          duration: {
            value: 0
          }
        },
        number: {
          value: 500,
          max: 0,
          density: {
            enable: true
          }
        },
        move: {
          gravity: {
            enable: false
          },
          decay: 0,
          direction: "bottom",
          speed: 2,
          outModes: "out"
        },
        size: {
          value: 12
        },
        opacity: {
          value: 1,
          animation: {
            enable: false
          }
        },
        rotate: {
          value: {
            min: 0,
            max: 360
          },
          direction: "random",
          move: true,
          animation: {
            enable: true,
            speed: 60
          }
        },
        tilt: {
          direction: "random",
          enable: true,
          move: true,
          value: {
            min: 0,
            max: 360
          },
          animation: {
            enable: true,
            speed: 60
          }
        },
        roll: {
          darken: {
            enable: true,
            value: 30
          },
          enlighten: {
            enable: true,
            value: 30
          },
          enable: true,
          mode: "both",
          speed: {
            min: 15,
            max: 25
          }
        },
        wobble: {
          distance: 30,
          enable: true,
          move: true,
          speed: {
            min: -15,
            max: 15
          }
        }
      },
      emitters: [],
      preset: "confetti",
      
    }
  });
})();

    </script>
</body>
</html>
