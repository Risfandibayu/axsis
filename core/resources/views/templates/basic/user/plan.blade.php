@extends($activeTemplate.'layouts.master')

@section('content')
<div class="row">
    @foreach($plans as $data)
    <div class="col-xl-4 col-md-4 mb-30">
        <div class="card rounded shadow border-0">
            <div class="card-body pt-5 pb-5 ">
                <div class="pricing-table text-center mb-4">
                    <h2 class="package-name mb-20 text-"><strong>@lang($data->name)</strong></h2>
                    <span class="price text--dark font-weight-bold d-block">{{$general->cur_sym}}{{getAmount($data->price)}}</span>
                    <hr>
                    <ul class="package-features-list mt-30">
                        <li><i class="las la-business-time __plan_info text--primary" data="bv"></i> <span>@lang('Business Volume (BV)'): {{getAmount($data->bv)}}</span> </li>
                        <li><i class="las la-comment-dollar __plan_info text--primary" data="ref_com"></i><span> @lang('Referral Commission'): {{$general->cur_sym}} {{getAmount($data->ref_com)}} </span>
                        </li>
                        <li>
                            <i class="las la-comments-dollar __plan_info text--primary" data="tree_com"></i> <span>@lang('Tree Commission'): {{$general->cur_sym}} {{getAmount($data->tree_com)}} </span>
                        </li>
                    </ul>
                </div>
                <div class="text-center">
                    {{-- @if(Auth::user()->plan_id != $data->id)
                    <a href="#" class="cmn--btn active __subscribe" data-id="{{ $data->id }}"><span>@lang('Subscribe')</span></a>
                    @else
                    <a class="cmn--btn active"><span>@lang('Already Subscribe')</span></a>
                    @endif --}}

                    @if(!Auth::user()->plan_id)
                    <a href="#confBuyModal{{$data->id}}" data-bs-toggle="modal"
                        class="btn w-100 btn-primary  mt-20 py-2 box--shadow1">@lang('Subscribe')</a>
                    @else
                    @if (Auth::user()->plan_id == $data->id)

                    <a data-toggle="modal" class="btn w-100 btn-success  mt-20 py-2 box--shadow1"><strong>@lang('
                            Subscribed')</strong></a>
                    {{-- <a href="#confBuyBRO{{$data->id}}" data-toggle="modal"
                        class="btn  w-100 btn--primary  mt-20 py-2 box--shadow1">@lang('Buy BRO')</a> --}}
                    @else
                    <a data-toggle="modal" class="btn w-100 btn-success  mt-20 py-2 box--shadow1">@lang('You
                        Already Subscribe Another Plan')</a>
                    @endif
                    @endif
                </div>
            </div>
         </div>
    </div>
    @endforeach
</div>

@endsection

@push('modal')

@foreach ($plans as $data)
<div class="modal fade" id="confBuyModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"> @lang('Confirm Purchase '.$data->name)?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <form method="post" action="{{route('user.plan.purchase')}}" id="form{{$data->id}}">
                {{-- <form method="post"> --}}
                    {{-- <div class="modal-body"> --}}
                        {{-- </div> --}}
                    @csrf
                    <div class="modal-body row">
                        <div class="form-group col-12">
                            <label for="">Total Transaction Available to Claim</label>
                            <input class="form-control" value="{{auth()->user()->total_invest - auth()->user()->total_invest_used}}" type="number"
                                placeholder="SP qty" readonly>
                        </div>
                        <h5 class="text-center col-12">{{getAmount($data->price)}} {{$general->cur_text}} / SP</h5>
                        <input type="hidden" class="prices" name="prices" value="{{getAmount($data->price)}}">
                        <input type="hidden" name="plan_id" value="{{$data->id}}">
                        <div class="form-group col-6">
                            <label for="">QTY</label>
                            <input class="form-control qty" type="number" name="qty" id="qty" min="1" value="{{floor((auth()->user()->total_invest - auth()->user()->total_invest_used) / 3000000)}}"
                                placeholder="SP qty" required>
                        </div>
                        <div class="form-group col-6">
                            <label for="">total</label>
                            <input class="form-control total" type="number" name="total"
                                value="{{getAmount($data->price * floor((auth()->user()->total_invest - auth()->user()->total_invest_used) / 3000000))}}" placeholder="total" disabled>
                        </div>
                        <div class="form-group col-12">
                            <label for="ref_name" class="form--label-2">@lang('Referral SP Number')</label>
                            <input type="text" name="referral"
                                class="referral ref_name form-control form--control-2" value="{{old('referral')}}"
                                id="ref_name" placeholder="@lang('Enter Referral SP Number')*" required>
                        </div>
                        {{-- <div class="col-6">
                            <label for="ref_name" class="form--label-2">@lang('Direct BRO Number')
                                <small>(Optional)</small></label>
                            <input type="text" name="upline" class="upline form-control form--control-2"
                                value="{{old('upline')}}" id="upline"
                                placeholder="@lang('Enter Direct BRO Number')">
                        </div> --}}
                        {{-- <div class="col-12">
                            <label for="ref_name" class="form--label-2">@lang('Select Position')</label>
                            <select name="position" class="position form-control form--control-2" id="position"
                                required disabled>
                                <option value="">@lang('Select position')*</option>
                                @foreach(mlmPositions() as $k=> $v)
                                <option value="{{$k}}">@lang($v)</option>
                                @endforeach
                            </select>
                            <span id="position-test" class="position-test">
                                <span class="text-danger">
                                    @if(!old('position'))
                                    @lang('Please enter Refferal SP Number first')
                                    @endif
                                </span>
                            </span>
                        </div> --}}
                        @if ($data->id ==1 || $data->id ==5 || $data->id == 8 || $data->id == 9)
                        <hr style="width: 100%; color: rgb(109, 108, 108);" />
                        <div class="col-12">
                            <label for="">Select Shipping Method</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="shipmethod" value="1"
                                    id="flexRadioDefault1{{$data->id}}" required>
                                <label class="form-check-label" for="flexRadioDefault1{{$data->id}}">
                                    Pick up at the office
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="shipmethod" value="2"
                                    id="flexRadioDefault2{{$data->id}}">
                                <label class="form-check-label" for="flexRadioDefault2{{$data->id}}">
                                    Delivery
                                </label>
                            </div>
                        </div>
                        <div class="col-12 shipping" id="shipping" style="display: none">
                            <label for="" class="form--label-2">Select SP Pack Shipping Address <small>(If empty
                                    add shipping address first in profile setting)</small></label>
                            <select name="alamat" id="alamat" class="form-control form--control-2 alamat">
                                <option value="" hidden selected>-- Select Address --</option>
                                @foreach ($alamat as $item)
                                <option value="{{$item->id}}">{{$item->nama_penerima .' | '.
                                    Str::limit($item->alamat,20)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 pickupdate" id="pickupdate" style="display: none">
                            <label for="" class="form--label-2">Select SP Pack Pick Up Date</label>
                            {{-- <input type="datetime-local" name="pickdate" class="form-control form--control-2"
                                min="{{now()->format('Y-m-d H:i')}}"> --}}

                            <div class="row">
                                <div class="col-6">
                                    <input type="date" name="pickdate" id="pickdate"
                                        class="form-control form--control-2 pickdate"
                                        min="{{now()->format('Y-m-d')}}">
                                </div>
                                <div class="col-6">
                                    <input type="text" name="picktime" id="picktime" autocomplete="off"
                                        class="form-control form--control-2 picktime timepicker" min="11:00"
                                        max="16:00" step="900" placeholder="11:00">
                                </div>
                            </div>
                            <span id="pick-error" class="pick-error" style="display: none">
                                <span class="text-danger">
                                    @lang('This day not allowed, please select only on Tuesday, Wednesday and
                                    Thursday')
                                </span>
                            </span>
                            <span id="pick-error2" class="pick-error2" style="display: none">
                                <span class="text-danger">
                                    @lang('This date and time has been booked by another user, please choose another
                                    date and time')
                                </span>
                            </span>

                        </div>

                        {{-- <select id="hours"></select>
                        <select id="minutes"></select> --}}
                        @endif

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--danger" data-dismiss="modal"><i
                                class="fa fa-times"></i>
                            @lang('Close')</button>

                        <button type="submit" class="btn btn--success sbt"><i class="lab la-telegram-plane"></i>
                            @lang('Subscribe')</button>
                    </div>
                </form>
        </div>
    </div>
</div>
@endforeach

<div class="modal fade" id="plan_info_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="plan_info_modal_title">@lang('Commission to tree info')</h5>

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer text-right ">
                <div class="row">
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-danger" id="__modal_close">@lang('Close')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="subscribe_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"> @lang('Confirm Purchase')?</h4>
            </div>
            <div class="modal-body">
                <h5 class="text-danger text-center">@lang('Are you sure to purchase?')</h5>
            </div>

            <div class="modal-footer">
                <form method="post" action="{{route('user.plan.purchase')}}">
                    @csrf
                    <input class="form-control form--control" type="hidden" class="d-none" name="plan_id" id="plan_id">
                    <button type="button" class="btn btn--dark" data-bs-dismiss="modal">@lang('Close')</button>
                    <button type="submit" class="btn btn--success "> @lang('Subscribe')</button>
                </form>
            </div>

        </div>
    </div>
</div>


@endpush

@push('script')
<script>
    'use strict';
    (function($) {
        $('.__plan_info').on('click', function(e) {
            let html = "";
            let data = $(this).attr('data');
            let modal = $("#plan_info_modal");
            console.log(data)
            if (data == 'bv') {
                html = ` <h5>   <span class="text-danger">@lang('When someone from your below tree subscribe this plan, You will get this Business Volume  which will be used for matching bonus').</span>
                </h5>`
                modal.find('#plan_info_modal_title').html("@lang('Business Volume (BV) info')")

            }
            if (data == 'ref_com') {
                html = `  <h5>  <span class=" text-danger">@lang('When Your Direct-Referred/Sponsored  User Subscribe in') <b> @lang('ANY PLAN') </b>, @lang('You will get this amount').</span>
                        <br>
                        <br>
                        <span class="text-success"> @lang('This is the reason You should Choose a Plan With Bigger Referral Commission').</span> </h5>`
                modal.find('#plan_info_modal_title').html("@lang('Referral Commission info')")

            }
            if (data == 'tree_com') {
                html = ` <h5 class=" text-danger">@lang('When someone from your below tree subscribe this plan, You will get this amount as Tree Commission'). </h5>`
                modal.find('#plan_info_modal_title').html("@lang('Referral Commission info')")

            }
            modal.find('.modal-body').html(html)
            $(modal).modal('show')
        });

        $('body').on('click', '#__modal_close', function(e) {
            $("#plan_info_modal").modal('hide');
        });


        $('.__subscribe').on('click', function(e) {
            let id = $(this).attr('data-id');
            $('#plan_id').attr('value', id);
            $("#subscribe_modal").modal('show');
        })
    })(jQuery)

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    (function($) {
        "use strict";

        var oldPosition = '{{ old("position") }}';

        if(oldPosition){
            $('select[name=position]').removeAttr('disabled');
            $('.position').val(oldPosition);
        }

        var not_select_msg = $('.position-test').html();

        $(document).on('blur', '.ref_name', function() {
            var ref_id = this.form.elements['ref_name'].value;
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('check.referralbro')}}",
                data: {
                    'ref_id': ref_id,
                    '_token': token
                },
                success: function(data) {
                    // console.log();
                    if (data.success) {
                        $('.position').removeAttr('disabled');
                        $('.position-test').text('');
                        // console.log(this.form.elements['ref_name'].value);
                    } else {
                        // console.log('ss');
                        $('.position').attr('disabled', true);
                        $('.position-test').html(not_select_msg);
                    }
                    $("#ref").html(data.msg);
                }
            });
        });

       

        $(document).on('change', '.position', function() {
            updateHand();
        });

        function updateHand() {
            var pos = $('.position').val();
            var referrer_id = $('.upline').val();
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('get.user.position')}}",
                data: {
                    'referrer': referrer_id,
                    'position': pos,
                    '_token': token
                },
                error: function(data) {
                    $(".position-test").html(data.msg);
                }
            });
        }

        @if(@$country_code)
        $(`option[data-code={{ $country_code }}]`).attr('selected', '');
        @endif
        $('select[name=country_code]').change(function() {
            $('input[name=country]').val($('select[name=country_code] :selected').data('country'));
        }).change();

        function submitUserForm() {
            var response = grecaptcha.getResponse();
            if (response.length == 0) {
                document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">@lang("Captcha field is required.")</span>';
                return false;
            }
            return true;
        }

        function verifyCaptcha() {
            document.getElementById('g-recaptcha-error').innerHTML = '';
        }

        @if($general -> secure_password)
        $('input[name=password]').on('input', function() {
            var password = $(this).val();
            var capital = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ]/;
            var capital = capital.test(password);
            if (!capital) {
                $('.capital').removeClass('text--success');
            } else {
                $('.capital').addClass('text--success');
            }
            var number = /[123456790]/;
            var number = number.test(password);
            if (!number) {
                $('.number').removeClass('text--success');
            } else {
                $('.number').addClass('text--success');
            }
            var special = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
            var special = special.test(password);
            if (!special) {
                $('.special').removeClass('text--success');
            } else {
                $('.special').addClass('text--success');
            }

        });
        @endif

        $('.qty').on('keyup change',function(e) { 
        // alert(this);
        // console.log(this.form.elements['total'].value);
        // this.form.find('.total').val()
        // $('.total').val($('.qty').val() * $('.prices').val());
        this.form.elements['total'].value = this.form.elements['qty'].value * this.form.elements['prices'].value;
        });

        $('input[name=qtyy]').on('keyup change',function() {
            // alert('okl');
            $('input[name=totall]').val($('input[name=qtyy]').val() * $('input[name=pricess]').val());
        });


        // $(document).on('click', '.sbt', function() {
        //     doSomething(this.form.id);
        //     // console.log(this.form.id);
        // });

        // function doSomething(id) {
        //     $("button[type=submit]").attr('disabled', 'disabled');
        // // do your heavy stuff here
        //     $("#"+id).submit();
        //     // console.log(this.form);
        // }

        $(document).on('click', '[name=shipmethod]', function() {
            ship(this.value);
            // console.log(this.value);
            $('select[name=alamat]').val("");
            $('input[name=pickdate]').val("");
            $('input[name=picktime]').val("");
        });


        function ship(id) {
                if (id == 2) {
                    var a = document.getElementsByClassName("shipping");
                    var b;
                    for (b = 0; b < a.length; b++) {
                        a[b].style.display = 'block';
                    }
                    var c = document.getElementsByClassName("pickupdate");
                    var d;
                    for (d = 0; d < c.length; d++) {
                        c[d].style.display = 'none';
                    }
                    // document.getElementById('shipping').style.display = 'block';
                    // document.getElementById('pickupdate').style.display = 'none';
                    // $('select[name=alamat]').attr('required', true);
                    $('input[name=pickdate]').attr('required', false);
                    $('input[name=picktime]').attr('required', false);
                }
                else {
                    var a = document.getElementsByClassName("shipping");
                    var b;
                    for (b = 0; b < a.length; b++) {
                        a[b].style.display = 'none';
                    }
                    var c = document.getElementsByClassName("pickupdate");
                    var d;
                    for (d = 0; d < c.length; d++) {
                        c[d].style.display = 'block';
                    }

                    // document.getElementById('shipping').style.display = 'none';
                    // document.getElementById('pickupdate').style.display = 'block';
                    $('select[name=alamat]').attr('required', false);
                    $('input[name=pickdate]').attr('required', true);
                    $('input[name=picktime]').attr('required', true);
                }
    
        };
        var picker = document.getElementsByClassName('pickdate');
        var pick = $('.pick-error').html();
        var b;
                for (b = 0; b < picker.length; b++) {
                        
                    picker[b].addEventListener('input', function(e){
                        var day = new Date(this.value).getUTCDay();
                        if([5,0,6,1].includes(day)){
                            e.preventDefault();
                            this.value = '';
                            // alert('This day not allowed');
                                var a = document.getElementsByClassName("pick-error");
                                var b;
                                for (b = 0; b < a.length; b++) {
                                    a[b].style.display = 'block';
                                }
                            // document.getElementById('pick-error').style.display = 'block';
                        }else{
                                var a = document.getElementsByClassName("pick-error");
                                var b;
                                for (b = 0; b < a.length; b++) {
                                    a[b].style.display = 'none';
                                }
                            // document.getElementById('pick-error').style.display = 'none';

                        }
                    });
        };
        
        let myTimepicker = document.getElementById("picktime");
        // myTimepicker.addEventListener("change", function() {
        
        //     console.log(`User changed the value to ${myTimepicker.value}`);
            
        //     let [hours, minutes] = myTimepicker.value.split(":");
            
        //     minutes = (Math.ceil(minutes / 15) * 15);
        //     if (minutes == 0) minutes = "00";
        //     if (minutes == 60) { minutes = "00"; ++hours % 24; }
                
        //     let newValue = hours + ":" + minutes;
            
        //     console.log(`Rounding value to ${newValue}`);
            
        //     myTimepicker.value = newValue;
        // });
        // myTimepicker.timepicker({ 'step': 15 });

    })(jQuery);

</script>
<script>
    $(document).ready(function(){
        $('input.timepicker').timepicker({ 
            zindex: 9999999,
            timeFormat: 'HH:mm',
            interval: 45,
            minTime: '11',
            maxTime: '16',
            startTime: '11:00',
            dynamic: false,
            dropdown: true,
            scrollbar: false,
            change: tm
        });


        function tm(){
            
        //  $(document).on('change', '.picktime', function() {
            var picktime = $('input[name=picktime]').val();
            var pickdate = $('input[name=pickdate]').val();
            // console.log('s');
            var token = "{{csrf_token()}}";
            $.ajax({
                type: "POST",
                url: "{{route('check.brodev')}}",
                data: {
                    'picktime': picktime,
                    'pickdate': pickdate,
                    '_token': token
                },
                success: function(data) {
                    // console.log();
                    if (data.success) {
                        // $('.position').removeAttr('disabled');
                        // $('.position-test').text('');
                        // // console.log(this.form.elements['ref_name'].value);\
                        // console.log('s');
                        $('input[name=pickdate]').val('');
                        $('input[name=picktime]').val('');
                        // document.getElementById('pick-error2').style.display = 'block';
                        var a = document.getElementsByClassName("pick-error2");
                        var b;
                        for (b = 0; b < a.length; b++) {
                            a[b].style.display = 'block';
                        }
                    } else {
                        // console.log('ss');
                        // document.getElementById('pick-error2').style.display = 'none';
                        var a = document.getElementsByClassName("pick-error2");
                        var b;
                        for (b = 0; b < a.length; b++) {
                            a[b].style.display = 'none';
                        }
                        // console.log('ss');
                        // $('.position').attr('disabled', true);
                        // $('.position-test').html(not_select_msg);
                    }
                    // $("#ref").html(data.msg);
                }
            });

        // });

        }

    });
</script>
{{-- <script>
    function createOption(value, text) {
           var option = document.createElement('option');
           option.text = text;
           option.value = value;
           return option;
    }

    var hourSelect = document.getElementById('hours');
    for(var i = 11; i <= 16; i++){
           hourSelect.add(createOption(i, i));
    }

    var minutesSelect = document.getElementById('minutes');
    for(var i = 0; i < 60; i += 15) {
           minutesSelect.add(createOption(i, i));
    }
</script> --}}
@endpush
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endpush
