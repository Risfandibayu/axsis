@extends($activeTemplate.'layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <form action="{{route('user.checkoutcart')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="table-responsive--sm table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col">@lang('#')</th>
                                    <th scope="col">@lang('Product')</th>
                                    <th scope="col" width="20%">@lang('Qty')</th>
                                    <th scope="col">@lang('Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($cart as $ex)
                                <tr>
                                    <td data-label="@lang('#')">{{ $cart->firstItem()+$loop->index }}</td>
                                    <td data-label="@lang('Product')">
                                        <div class="row d-flex">
                                            <div class="book">
                                                <img src="{{getImage(imagePath()['products']['path'].'/'.$ex->product->thumbnail,imagePath()['products']['size'])}}"
                                                    class="book-img" style="max-width: 50%">
                                            </div>
                                            <div class="my-auto flex-column d-flex pad-left">
                                                <h6 class="mob-text font-weight-bold ml-2">Product Name : {{
                                                    $ex->product->name }} </h6>
                                            </div>
                                        </div>

                                    </td>
                                    <td data-label="@lang('Qty')">
                                        {{-- {{ $ex->qty }} --}}
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number"
                                                    data-type="minus"
                                                    data-field="quant[{{ $cart->firstItem()+$loop->index }}]">
                                                    <i class="las la-minus"></i>
                                                </button>
                                            </span>
                                            <input type="hidden" name="id[]" value="{{ $ex->id }}">
                                            <input type="text" name="quant[{{ $cart->firstItem()+$loop->index }}]"
                                                class="form-control input-number" value="{{ $ex->qty }}" min="1"
                                                max="{{$ex->product->quantity}}">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number"
                                                    data-type="plus"
                                                    data-field="quant[{{ $cart->firstItem()+$loop->index }}]">
                                                    <i class="las la-plus"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td data-label="@lang('Action')">
                                        {{-- <form action="{{route('user.gold.deletecart', $ex->id)}}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <button type="submit">
                                                X
                                            </button>
                                        </form> --}}
                                        <a href="{{route('user.delCart', $ex->id)}}" class="btn">×</a>
                                    </td>

                                </tr>
                                @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{-- <div class="row col-lg-12 mb-3">

                            <div class="col-lg-8"></div>
                            <div class="row col-lg-4">
                                <div class="col-8">

                                    <small class="c-cart-float-r text-center ">Click the <strong>Update Cart</strong>
                                        button
                                        when adding qty to cart</small>
                                </div>
                                <div class="col-4 justify-content-end">
                                    <button type="submit" class="btn btn--info btn-block btn-upd"
                                        disabled="disabled">Update Cart</button>
                                </div>

                            </div>
                        </div> --}}
                    </div>
                </div>
                @if (count($cart) > 0)
                <hr style="border: 1px solid rgb(83, 83, 83);">
                <div class="card-body mb-3 row justify-content-end">
                    <div class="col-md-6">

                    <div class="card">

                        <div class="card-body">
                        <h5 class="card-title">Checkout</h5>

                        <div class="form-group col-md-12">
                            <label for="" class="font-weight-bold">Select Shipping
                                Address</label>
                                <select name="alamat" id="alamat" class="form-control form-control-lg" required>
                                    {{-- <option value="" hidden selected>-- Select Address --</option> --}}
                                    @foreach ($alamat as $item)
                                    <option @if ($item->selected == 1)
                                        selected
                                    @endif value="{{$item->id}}">{{$item->nama_penerima .' | '.
                                    Str::limit($item->alamat,20)}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="form-group col-md-12">
                            <label for="" class="font-weight-bold">Select Payment Method</label>
                                <select name="alamat" id="alamat" class="form-control form-control-lg" required>
                                    <option value="" hidden selected>-- Select Address --</option>
                                    @foreach ($alamat as $item)
                                    <option value="{{$item->id}}">{{$item->nama_penerima .' | '.
                                    Str::limit($item->alamat,20)}}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        <div class="row justify-content-end">
                            <div class="col-md-12"> 
                            <button type="submit" class="btn btn-lg btn--info" style="width: 100%;" @if (count($cart)==0 ) disabled
                            @endif>Confirm</button>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

@endsection

@push('script')
<script>
    $('.btn-number').click(function(e){
    e.preventDefault();
    $('.btn-upd').attr('disabled', false);
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
@endpush