@extends($activeTemplate.'layouts.master')
@section('content')
<h4 class="mb-2">@lang('Orders')</h4>
<table class="transection-table-2">
    <thead>
        <tr>
            <th>@lang('TRX')</th>
            {{-- <th>@lang('Quantity')</th> --}}
            {{-- <th>@lang('Price')</th> --}}
            <th>@lang('Date')</th>
            <th>@lang('Total')</th>
            <th>@lang('Shipping Address')</th>
            <th>@lang('Status')</th>
            <th>@lang('Payment Status')</th>
            <th>@lang('Detail')</th>
        </tr>
    </thead>
    <tbody>
        @forelse($orders as $order)
        <tr>
            {{-- <td data-label="@lang('Product')">{{ __($order->product->name) }}</td> --}}
            {{-- <td data-label="@lang('Quantity')">{{ __($order->quantity) }}</td> --}}
            {{-- <td data-label="@lang('Price')">{{ showAmount($order->price) }} {{ $general->cur_text }}</td> --}}
            <td data-label="@lang('Product')">{{ __($order->trx) }}</td>
            <td data-label="@lang('Date')">{{ showDateTime($order->created_at) }}</td>
            <td data-label="@lang('Total Price')">{{ nb($order->total) }} {{ $general->cur_text }}</td>
            <td data-label="@lang('Shipping Address')">{{ Str::limit($order->alamat,40) }}<button
                    class="btn--info btn-rounded  badge detailAlm" data-alamat="{{$order->alamat}}"
                    data-pos="{{$order->kode_pos}}" data-nhp="{{$order->no_telp_penerima}}"
                    data-np="{{$order->nama_penerima}}"><i class="fa fa-info"></i></button></td>
            <td data-label="@lang('Status')">
                @if($order->status == 0)
                <span class="badge bg--warning">@lang('Pending')</span>
                @elseif($order->status == 1)
                <span class="badge bg--success">@lang('Shipped')</span>
                @if($order->no_resi != null)
                <button class="btn--info btn-rounded  badge detailBtn" data-no_resi="{{$order->no_resi}}"><i
                        class="fa fa-info"></i></button>
                @endif
                @else
                <span class="badge bg--danger">@lang('Cancelled')</span>
                @endif

            </td>
            <td>
                @if($order->payment_status == 2)
                <span class="badge bg--warning">@lang('Pending')</span>
                @elseif($order->payment_status == 1)
                <span class="badge bg--success">@lang('Success')</span>
                @else
                <span class="badge bg--danger">@lang('Cancelled')</span>
                @endif
            </td>
            <td data-label="@lang('Detail')"><a href="" class="badge btn-primary det" id="det{{$order->id}}"
                    data-bs-toggle="modal" data-bs-target="#detailprod{{$order->id}}"><i
                        class="las la-info"></i>Detail</a></td>
        </tr>
        @empty
        <tr>
            <td colspan="100%" class="text-center">@lang('No order found')</td>
        </tr>
        @endforelse
    </tbody>
</table>
{{ $orders->links() }}
@endsection

@push('modal')
@foreach ($orders as $item)
<div id="detailprod{{$item->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- @foreach ($ex->detail as $item)

                @endforeach --}}
                <table class="table table--light style--two">
                    <thead>
                        <tr>
                            {{-- <th scope="col">@lang('#')</th> --}}
                            <th scope="col">@lang('Product')</th>
                            <th scope="col" width="20%">@lang('Qty')</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($item->detail as $ex)
                        <tr>
                            {{-- <td data-label="@lang('#')">{{ $devcart->firstItem()+$loop->index }}</td> --}}
                            <td data-label="@lang('Product')">

                                <div class="row d-flex">
                                    <div class="book">
                                        <img src="{{ getImage('assets/images/products/'. $ex->product->thumbnail,  null, true)}}"
                                            class="book-img">
                                    </div>
                                    <div class="my-auto flex-column d-flex pad-left">
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                            $ex->product->name }} </h6>
                                    </div>
                                </div>

                            </td>
                            <td data-label="@lang('Qty')">
                                {{-- {{ $ex->qty }} --}}
                                <div class="input-group">

                                    <input type="text" readonly class="form-control input-number"
                                        value="{{ $ex->qty }} pcs" min="1" max="{{$ex->product->quantity}}">

                                </div>
                            </td>



                        </tr>
                        @empty
                        <tr>
                            <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endforeach

<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Nomor Resi : <span class="withdraw-detail"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>

<div id="detailAlm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Alamat : <span class="withdraw-detail"></span>. <br> Kode Pos : <span
                        class="withdraw-detail1"></span>
                    <br> Nama Penerima : <span class="withdraw-detail3"></span>
                    <br> No Telp Penerima : <span class="withdraw-detail2"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
@endpush

@push('script')
<script>
    $('.detailBtn').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('no_resi');
                modal.find('.withdraw-detail').html(feedback);
                modal.modal('show');
    });
</script>
<script>
    $('.detailAlm').on('click', function () {
            var modal = $('#detailAlm');
            var feedback = $(this).data('alamat');
            var pos = $(this).data('pos');
            var nhp = $(this).data('nhp');
            var np = $(this).data('np');
            modal.find('.withdraw-detail').html(feedback);
            modal.find('.withdraw-detail1').html(pos);
            modal.find('.withdraw-detail2').html(nhp);
            modal.find('.withdraw-detail3').html(np);
            modal.modal('show');
    });
</script>
@endpush