@extends($activeTemplate.'layouts.master')

@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
<style>
    :root {
        --primary-color: rgb(11, 78, 179)
    }


    label {
        display: block;
        margin-bottom: 0.5rem
    }

    input {
        display: block;
        width: 100%;
        padding: 0.75rem;
        border: 1px solid #ccc;
        border-radius: 0.25rem;
        height: 50px
    }

    .width-50 {
        width: 50%
    }

    .ml-auto {
        margin-left: auto
    }

    .text-center {
        text-align: center
    }

    .progressbar {
        position: relative;
        display: flex;
        justify-content: space-between;
        counter-reset: step;
        margin: 2rem 2rem 4rem
    }

    .progressbar::before,
    .progress {
        content: "";
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        height: 4px;
        width: 100%;
        background-color: #dcdcdc;
        z-index: 1
    }
    .progressbar::before,
    .progress2 {
        content: "";
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        height: 4px;
        width: 100%;
        background-color: #dcdcdc;
        z-index: 1
    }

    .progress {
        background-color: rgb(0 128 0);
        width: 0%;
        transition: 0.3s
    }
    .progress2 {
        background-color: rgb(0 128 0);
        width: 0%;
        transition: 0.3s
    }

    .progress-step {
        width: 2.1875rem;
        height: 2.1875rem;
        background-color: #dcdcdc;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1
    }
    .progress-step2 {
        width: 2.1875rem;
        height: 2.1875rem;
        background-color: #dcdcdc;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1
    }

    .progress-step::before {
        counter-increment: step;
        content: counter(step);
            /* font-family: FontAwesome; */
            /* content: "\f023"; */

    }
    .progress-step2::before {
        counter-increment: step;
        content: attr(data-value);
        font-family: "Fira Sans Condensed", sans-serif;
        font-size: 0.95rem;
        font-weight: 900;
            /* font-family: FontAwesome; */
            /* content: "\f023"; */

    }

    .progress-step::after {
        content: attr(data-title);
        position: absolute;
        top: calc(100% + 0.5rem);
        font-size: 0.85rem;
        color: #666;
        width: 80px;
    }
    .progress-step2::after {
        content: attr(data-title) "\A" attr(data-title2);
        white-space: pre;
        position: absolute;
        top: calc(100% + 0.5rem);
        font-family: "Fira Sans Condensed", sans-serif;
        font-size: 0.95rem;
        font-weight: 900;
        line-height:20px;
        color: #666;
        width: 80px;
    }

    .progress-step-active {
        background-color: var(--primary-color);
        color: #f3f3f3
    }

    .form {
        width: clamp(320px, 30%, 430px);
        margin: 0 auto;
        border: none;
        border-radius: 10px !important;
        overflow: hidden;
        padding: 1.5rem;
        background-color: #fff;
        padding: 20px 30px
    }

    .step-forms {
        display: none;
        transform-origin: top;
        animation: animate 1s
    }

    .step-forms-active {
        display: block
    }

    .group-inputs {
        margin: 1rem 0
    }

    @keyframes animate {
        from {
            transform: scale(1, 0);
            opacity: 0
        }

        to {
            transform: scale(1, 1);
            opacity: 1
        }
    }

    .btns-group {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        gap: 1.5rem
    }

    .btn {
        padding: 0.75rem;
        display: block;
        text-decoration: none;
        background-color: var(--primary-color);
        color: #f3f3f3;
        text-align: center;
        border-radius: 0.25rem;
        cursor: pointer;
        transition: 0.3s
    }

    .btn:hover {
        box-shadow: 0 0 0 2px #fff, 0 0 0 3px var(--primary-color)
    }

    .progress-step-check {
        position: relative;
        background-color: green !important;
        transition: all 0.8s
    }

    .progress-step-check::before {
        position: absolute;
        content: '\2713';
        width: 100%;
        height: 100%;
        top: 8px;
        left: 13px;
        font-size: 12px
    }

    .group-inputs {
        position: relative
    }

    .group-inputs label {
        font-size: 13px;
        position: absolute;
        height: 19px;
        padding: 4px 7px;
        top: -14px;
        left: 10px;
        color: #a2a2a2;
        background-color: white
    }

    .welcome {
        height: 450px;
        width: 350px;
        background-color: #fff;
        border-radius: 6px;
        display: flex;
        justify-content: center;
        align-items: center
    }

    .welcome .content {
        display: flex;
        align-items: center;
        flex-direction: column
    }

    .checkmark__circle {
        stroke-dasharray: 166;
        stroke-dashoffset: 166;
        stroke-width: 2;
        stroke-miterlimit: 10;
        stroke: #7ac142;
        fill: none;
        animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards
    }

    .checkmark {
        width: 56px;
        height: 56px;
        border-radius: 50%;
        display: block;
        stroke-width: 2;
        stroke: #fff;
        stroke-miterlimit: 10;
        margin: 10% auto;
        box-shadow: inset 0px 0px 0px #7ac142;
        animation: fill .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both
    }

    .checkmark__check {
        transform-origin: 50% 50%;
        stroke-dasharray: 48;
        stroke-dashoffset: 48;
        animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards
    }

    @keyframes stroke {
        100% {
            stroke-dashoffset: 0
        }
    }

    @keyframes scale {

        0%,
        100% {
            transform: none
        }

        50% {
            transform: scale3d(1.1, 1.1, 1)
        }
    }

    @keyframes fill {
        100% {
            box-shadow: inset 0px 0px 0px 30px #7ac142
        }
    }
    .custom-btn {
        /* width: 130px; */
        /* height: 40px; */
        color: #fff;
        border-radius: 5px;
        /* padding: 10px 25px; */
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: transparent;
        /* cursor: pointer; */
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgba(255, 255, 255, .5),
            7px 7px 20px 0px rgba(0, 0, 0, .1),
            4px 4px 5px 0px rgba(0, 0, 0, .1); */
        outline: none;
    }

    /* 11 */
    .btn-11 {
        display: inline-block;
        outline: none;
        font-family: inherit;
        font-size: 1em;
        box-sizing: border-box;
        border: none;
        border-radius: .3em;
        /* height: 2.75em; */
        /* line-height: 2.5em; */
        text-transform: uppercase;
        /* padding: 0 1em; */
        /* box-shadow: 0 3px 6px rgba(0, 0, 0, .16), 0 3px 6px rgba(110, 80, 20, .4),
            inset 0 -2px 5px 1px rgba(139, 66, 8, 1),
            inset 0 -1px 1px 3px rgba(250, 227, 133, 1); */
        background-image: linear-gradient(160deg, #a54e07, #dda431, #eedd6d, #e2aa2f, #a54e07);
        /* border: 1px solid #a55d07; */
        color: rgb(120, 50, 5);
        text-shadow: 0 2px 2px rgba(250, 227, 133, 1);
        /* cursor: pointer; */
        transition: all .2s ease-in-out;
        background-size: 100% 100%;
        background-position: center;
        overflow: hidden;
    }

    /* .btn-11:hover {
    text-decoration: none;
    color: #fff;
} */
    .btn-11:before {
        position: absolute;
        content: '';
        display: inline-block;
        top: -180px;
        left: 0;
        width: 30px;
        height: 100%;
        background-color: #fff;
        animation: shiny-btn1 3s ease-in-out infinite;
    }

    /* .btn-11:hover{
  opacity: .7;
} */
    /* .btn-11:active{
  box-shadow:  4px 4px 6px 0 rgba(255,255,255,.3),
              -4px -4px 6px 0 rgba(116, 125, 136, .2), 
    inset -4px -4px 6px 0 rgba(255,255,255,.2),
    inset 4px 4px 6px 0 rgba(0, 0, 0, .2);
} */


    @-webkit-keyframes shiny-btn1 {
        0% {
            -webkit-transform: scale(0) rotate(45deg);
            opacity: 0;
        }

        80% {
            -webkit-transform: scale(0) rotate(45deg);
            opacity: 0.5;
        }

        81% {
            -webkit-transform: scale(4) rotate(45deg);
            opacity: 1;
        }

        100% {
            -webkit-transform: scale(50) rotate(45deg);
            opacity: 0;
        }
    }

    .desktop-img {
			display: block;
			width: 100%;
			height: auto;
		}

		/* gambar untuk tampilan seluler */
		.mobile-img {
			display: none;
		}

		/* media query untuk tampilan seluler */
		@media only screen and (max-width: 600px) {
			.desktop-img {
				display: none;
			}

			.mobile-img {
				display: block;
				width: 100%;
				height: auto;
			}
		}

        .hidden-mobile {
        display: none;
        }

        /* Gunakan media query untuk menunjukkan elemen yang disembunyikan pada layar yang lebih besar dari ukuran tertentu */
        @media (min-width: 768px) {
            /* Sesuaikan ukuran batas sesuai kebutuhan Anda */
            .hidden-mobile {
                display: block;
            }
        }

        .star {
            font-size: 40px;
            display: inline-block;
            color: rgb(119, 0, 255);
        }

        .title {
            font-size: 20px;
            align-content: center;
            font-family: "Fira Sans Condensed", sans-serif;
            font-weight: 900;
        }
        .mb-pg {
                display: flex;
            }

        @media (max-width: 768px) {
            /* Sesuaikan ukuran batas sesuai kebutuhan Anda */
            .mb-pg {
                display: none;
            }
        }
</style>
@endpush
@section('content')
@if (auth()->user()->userExtra->jabatan_id != 0)
<div class="row g-3 mb-3 ">
    <div class="col-lg-12 col-md-12 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
                Account Level
            </div>
            <div class="card-body text-center align-content-center">
                <div class="card-title">
                    <i class="flaticon-star-2 star"> </i> <br> <span class="title">{{auth()->user()->userExtra->jabatan->name}} • {{auth()->user()->userExtra->count}} SP</span>
                </div>
                <div class="progressbar mb-pg">
                    <div class="progress2" id="progress2"></div>
                    @foreach ($jabatan as $index => $item)
                        <div class="progress-step2 {{ (auth()->user()->userExtra->count >= $item->req) ? 'progress-step-active' : '' }}" data-value="{{$item->req}}" data-title="{{ $item->name }}" data-title2="{{ $item->req. ' SP' }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row g-3 mb-3">
    {{-- @if (Auth::user()->plan_id != 0) --}}
    
    <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
            Saka Vaganza
        </div>
        <div class="card-body fs-1 text-center " style="border-radius: 0 0 6px 6px;background-image: url('{{ getImage('assets/images/logo_saka.png',  null, true)}}');    background-size: cover; background-position: center;">
            {{-- <h2 style="font-weight: 700;color: black;line-height: 5; font-size: 33px;">{{Auth::user()->no_bro}}</h2> --}}
            <div class="row justify-content-center p-3">
                <div class="col-8">
                    <h2 class="text-light font-weight-bold" style="text-shadow: 2px 0 0px #800040, 3px 2px 0px rgba(77,0,38,0.5), 3px 0 3px #FF002B, 5px 0 3px #800015, 6px 2px 3px rgba(77,0,13,0.5), 6px 0 9px #FF5500, 8px 0 9px #802A00, 9px 2px 9px rgba(77,25,0,0.5), 9px 0 18px #FFD500, 11px 0 18px #806A00, 12px 2px 18px rgba(77,66,0,0.5), 12px 0 30px #D4FF00, 14px 0 30px #6A8000, 15px 2px 30px rgba(64,77,0,0.5), 15px 0 45px #80FF00, 17px 0 45px #408000, 17px 2px 45px rgba(38,77,0,0.5);">
                        {{Auth::user()->gacha_qty}}
                        TICKET</h2>
                </div>
                <div class="col-8">
                    {{-- <form method="post" action="{{route('user.gacha.store')}}" enctype="multipart/form-data">
                        @csrf
                        <button type="submit" class="btn btn-sm btn-light btn-block text-uppercase font-weight-bold">Redeem Now</button>
                    </form> --}}
                    @if ($saka->status == 0)
                    <a class="btn btn-sm btn-light btn-block text-uppercase font-weight-bold ainfo2" >Redeem Now</a>                                
                    @else
                    <a class="btn btn-sm btn-light btn-block text-uppercase font-weight-bold" href="{{route('user.saka.vaganza')}}" target="_blank">Redeem Now</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
    {{-- <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
            Gebyar Dinaran
        </div>
        <div class="card-body fs-1 text-center " style="border-radius: 0 0 6px 6px;background-image: url('{{ getImage('assets/images/FILI-GOT-IT.png',  null, true)}}');    background-size: cover; background-position: center;">
            <div class="row justify-content-center p-3">
                <div class="col-8">
                    <h2 class="text-light font-weight-bold" style="text-shadow: 2px 0 0px #800040, 3px 2px 0px rgba(77,0,38,0.5), 3px 0 3px #FF002B, 5px 0 3px #800015, 6px 2px 3px rgba(77,0,13,0.5), 6px 0 9px #FF5500, 8px 0 9px #802A00, 9px 2px 9px rgba(71, 25, 3, 0.5), 9px 0 18px #FFD500, 11px 0 18px #806A00, 12px 2px 18px rgba(77,66,0,0.5), 12px 0 30px #D4FF00, 14px 0 30px #6A8000, 15px 2px 30px rgba(64,77,0,0.5), 15px 0 45px #80FF00, 17px 0 45px #408000, 17px 2px 45px rgba(38,77,0,0.5);">
                        {{Auth::user()->gacha_qty}}
                        TICKET</h2>
                </div>
                <div class="col-8">
                    @if ($filigotit->status == 0)
                    <a class="btn btn-sm btn-light btn-block text-uppercase font-weight-bold ainfo" >Redeem Now</a>                                
                    @else
                    <a class="btn btn-sm btn-light btn-block text-uppercase font-weight-bold" href="{{route('user.gacha.fili.got.it')}}" target="_blank">Redeem Now</a>
                    @endif
                </div>
            </div>
        </div>
    </div> --}}
</div>
{{-- @endif --}}

    @if(Auth::user()->is_kyc == 0)
    {{-- <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step" data-title="Verification"></div>
                    <div class="progress-step" data-title="Status"></div>
                </div>
                <p>It is recommended to verify to be able to unlock all features.</p>
                <a href="{{route('user.verification')}}" class="btn btn-sm btn-danger">Verify Now</a>
            </div>
        </div>
    </div> --}}
    @elseif(Auth::user()->is_kyc == 1)
    <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step" data-title="Status"></div>
                </div>
                <p>Your data is in the verification process.</p>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->is_kyc == 2)
    {{-- <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step progress-step-active bg-success" data-title="Verified"></div>
                </div>
                <p>Your data has been successfully verified.</p>
            </div>
        </div>
    </div> --}}
    @elseif(Auth::user()->is_kyc == 3)
    <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
                Account Verification
            </div>
            <div class="card-body text-center">
                <div class="progressbar">
                    <div class="progress" id="progress"></div>
                    <div class="progress-step progress-step-active" data-title="Unverified"></div>
                    <div class="progress-step progress-step-active" data-title="Verification"></div>
                    <div class="progress-step progress-step-active bg-danger" data-title="Rejected"></div>
                </div>
                <p>Your data failed to verify, please resend your data.</p>
                <p> <small>See KYC Reject Reason </small> <button class="btn-info btn-rounded badge detail" data-note="{{Auth::user()->kyc_note}}"><i
                    class="fa fa-info"></i></button> </p> 
                <a href="{{route('user.verification')}}" class="btn btn-sm btn-danger">Resend Data</a>
            </div>
        </div>
    </div>
    @endif

    @if (Auth::user()->plan_id != 0)
    <div class="col-lg-4 col-md-4 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100 dashboard-item">
            <div class="card-header" style="font-weight: 600;">
                SIS Number
            </div>
            <div class="card-body fs-1 text-center bg--gradi-9" style="border-radius: 0 0 6px 6px;">
                <h2 style="font-weight: 700;color: black;line-height: 5; font-size: 33px;">{{Auth::user()->no_bro}}</h2>
            </div>
        </div>
    </div>
    @endif
    {{-- <div class="col-lg-3 col-md-3 col-12 mb-30">
        <div class="card card-header-actions h-100 w-100">
            <div class="card-header" style="font-weight: 600;">
                Title
            </div>
            <div class="card-body text-center custom-btn btn-11" style="border-radius: 0 0 6px 6px;">
                <img class="img-fluid w-50" src="{{ getImage('assets/images/leader.png', null, true) }}" alt="">
            </div>
        </div>
    </div> --}}
</div>
{{-- <div class="row">
    @if($general->notice != null)
        <div class="col-12 mb-30">
            <div class="card border--light mb-4">
                <div class="card-header">@lang('Notice')</div>
                <div class="card-body">
                    <p class="card-text">@php echo $general->notice; @endphp</p>
                </div>
            </div>
        </div>
    @endif
    @if($general->free_user_notice != null)
        <div class="col-12 mb-30">
            <div class="card border--light mb-2">
                @if($general->notice == null)
                    <div class="card-header">@lang('Notice')</div>   @endif
                <div class="card-body">
                    <p class="card-text"> @php echo $general->free_user_notice; @endphp </p>
                </div>
            </div>
        </div>
    @endif
</div> --}}

<div class="row justify-content-center g-3">
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total SP')</h6>
                    <h3 class="ammount theme-one">{{nb(auth()->user()->userExtra->count)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-star-2"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Current Balance')</h6>
                    <h3 class="ammount theme-two">{{$general->cur_sym}}{{nb(auth()->user()->balance)}}</h3>
                </div>
                <div class="right-content">
                    <div class="icon"><i class="flaticon-wallet"></i></div>
                </div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total Deposit')</h6>
                    <h3 class="ammount text--base">{{$general->cur_sym}}{{nb($totalDeposit)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-save-money"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total Withdraw')</h6>
                    <h3 class="ammount theme-one">{{$general->cur_sym}}{{nb($totalWithdraw)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-withdraw"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Complete Withdraw')</h6>
                    <h3 class="ammount theme-two">{{$general->cur_sym}}{{nb($completeWithdraw)}}</h3>
                </div>
                <div class="right-content">
                    <div class="icon"><i class="flaticon-wallet"></i></div>
                </div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Pending Withdraw')</h6>
                    <h3 class="ammount text--base">{{$general->cur_sym}}{{nb($pendingWithdraw)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-withdrawal"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Reject Withdraw')</h6>
                    <h3 class="ammount theme-one">{{$general->cur_sym}}{{nb($rejectWithdraw)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-fees"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total Transaction')</h6>
                    <h3 class="ammount theme-one">{{$general->cur_sym}}{{nb(auth()->user()->total_invest)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-tag-1"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div>
    {{-- <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total Referral Commission')</h6>
                    <h3 class="ammount theme-one">${{getAmount(auth()->user()->total_ref_com)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-clipboards"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div> --}}
    {{-- <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
        <div class="dashboard-item">
            <div class="dashboard-item-header">
                <div class="header-left">
                    <h6 class="title">@lang('Total Binary Commission')</h6>
                    <h3 class="ammount theme-one">${{getAmount(auth()->user()->total_binary_com)}}</h3>
                </div>
                <div class="icon"><i class="flaticon-money-bag"></i></div>
            </div>
            <div class="dashboard-item-body">
            </div>
        </div>
    </div> --}}
    
</div>

@push('modal')
<div class="modal fade" id="Ainfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Fili Got It</h5>
            </div>
            <div class="modal-body px-4 py-4 text-center">
                <p>Gebyar FILI GOT IT belum di mulai, Silahkan Tunggu</p>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="Ainfo2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Saka Vaganza</h5>
            </div>
            <div class="modal-body px-4 py-4 text-center">
                <p>Saka Vaganza belum di mulai, Silahkan Tunggu</p>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="detailModal" style="z-index: 9999999999 !important">
    <div class="modal-dialog" role="document" style="z-index: 9999999999 !important">
        <div class="modal-content" style="z-index: 9999999999 !important">
            <div class="modal-header">
                <h5 class="modal-title">@lang('KYC Reject Reason')</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="detail_note"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
<div id="add-address" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Address')</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('user.add_address')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s name')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control" name="nama_penerima" placeholder="Nama Penerima"
                            required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Recipient`s phone number')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <input type="text" class="form-control" name="no_telp" placeholder="Nomor Telepon Penerima"
                            required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Address')</label>
                        {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                            onchange="loadFile(event)" name="images" required> --}}
                        <textarea style="border:inline" class="form-control" id="alamat" name="alamat" rows="4" placeholder="Jalan, No Rumah, RT/RW, Desa/Dusun"
                            required></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Districts')</label>
                            <input type="text" class="form-control" name="kec" placeholder="Kecamatan" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('City')</label>
                            <input type="text" class="form-control" name="kab" placeholder="Kota/Kabupaten" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Province')</label>
                            <input type="text" class="form-control" name="prov" placeholder="Provinsi" required>
                        </div>
                        <div class="form-group col-6">
                            <label class="font-weight-bold"> @lang('Postal Code')</label>
                            {{-- <input class="form-control form-control-lg" type="file" accept="image/*"
                                onchange="loadFile(event)" name="images" required> --}}
                            <input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos Penerima"
                                required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endpush

@endsection

@push('script')
    <script>
        'use strict';
        (function($){
            $('.detail').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('note');

                modal.find('.detail_note').html(`<p> ${feedback} </p>`);
                modal.modal('show');
            });
            $('.ainfo').on('click', function () {
                var modal = $('#Ainfo');
                modal.modal('show');
            });
            $('.ainfo2').on('click', function () {
                var modal = $('#Ainfo2');
                modal.modal('show');
            });
        })(jQuery)
    </script>
    {{-- <script type="text/javascript">
        $(window).on('load', function() {
            $('#myModal').modal('show');
        });
      </script> --}}
    @if ($alamat->isEmpty())
        
    <script type="text/javascript">
        $(window).on('load', function() {
            $('#add-address').modal('show');
            iziToast.warning({
            message: 'Please enter the shipping address',
            position: "topRight"
        });
        });
    </script>
    @endif
@endpush


