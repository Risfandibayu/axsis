@extends($activeTemplate.'layouts.frontend')
@section('content')
@include($activeTemplate.'layouts.breadcrumb')

<!-- Product Details Section Starts Here -->
<section class="product-details padding-top padding-bottom pos-rel">
    <div class="container">
        <div class="row gy-4 gy-sm-5">
            <div class="col-lg-5">
                <div class="product-thumb-wrapper">
                    <div class="sync1 owl-carousel owl-theme">
                        <div class="thumbs">
                            <img class="zoom_img" src="{{getImage(imagePath()['products']['path'].'/'.$product->thumbnail,imagePath()['products']['size'])}}" alt="products-details">
                        </div>
                        @if ($product->images != null)
                        @foreach ($product->images as $image)
                        <div class="thumbs"> <img class="zoom_img" src="{{getImage(imagePath()['products']['path'].'/'.$image,imagePath()['products']['size'])}}" alt="products-details"></div>
                        @endforeach
                        @endif
                    </div>
                    <div class="sync2 owl-carousel owl-theme mt-2">
                        <div class="thumbs">
                            <img class="zoom_img" src="{{getImage(imagePath()['products']['path'].'/'.$product->thumbnail,imagePath()['products']['size'])}}" alt="products-details">
                        </div>
                        @if ($product->images != null)
                        @foreach ($product->images as $image)
                        <div class="thumbs">
                            <img src="{{getImage(imagePath()['products']['path'].'/'.$image,imagePath()['products']['size'])}}" alt="products-details">
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="product-info-wrapper">
                    <h3 class="title">{{__(@$product->name)}}</h3>
                    <div class="product-price">
                        @if (!Auth::check())
                        <span class="current-price">{{nb($product->price_public)}} {{ $general->cur_text }}</span>
                        @else
                            @if (firstrx2(Auth::user()->id) == true )
                            <span class="current-price">{{nb($product->price_public)}} {{ $general->cur_text }}</span>
                            @else
                            <span class="current-price">  {{nb($product->price)}} {{ $general->cur_text }}</span><br>
                            <span class="current-price-slice" style="text-decoration: line-through;">  {{nb($product->price_public)}} {{ $general->cur_text }}</span>
                            @endif
                        @endif
                    </div>
                    @if($product->quantity > 0)
                    <span class="custom--badge bg--success mt-3">@lang('In Stock')</span>
                    @else
                    <span class="custom--badge bg--danger mt-3">@lang('Out of Stock')</span>
                    @endif
                    <div class="add-cart-wrapper">
                        <div class="cart-plus-minus">
                            <div class="cart-decrease qtybutton dec"><i class="las la-arrow-left"></i></div>
                            <input class="cart-count" type="number" value="1" min="1" max="{{$product->quantity}}" readonly>
                            <div class="cart-increase qtybutton inc active"><i class="las la-arrow-right"></i></div>
                        </div>
                        @if($product->quantity > 0)
                        {{-- <a href="javascript:void(0)" class="cart--btn cmn--btn-2 bg--primary cartBtn" data-id="{{ $product->id }}" data-name="{{ $product->name }}">@lang('Add To Cart')</a> --}}
                            @if (!Auth::check())
                                <a href="{{ route("user.register") }}?ref={{$ref}}" class="cart--btn cmn--btn-2 bg--primary">@lang('Add To Cart')</a>
                            @else
                            <a href="javascript:void(0)" class="cart--btn cmn--btn-2 bg--primary cartform" data-id="{{ $product->id }}">@lang('Add To Cart')</a>
                            <form action="{{ route("user.addToCart") }}" method="post" id="cartform">
                                @csrf
                                <input type="hidden" name="qty" class="qty" id="qty">
                                <input type="hidden" name="product_id" class="product_id" id="product_id">
                                <input type="hidden" name="referral" value="{{$ref}}" class="referral" id="referral">
                            </form>
                            {{-- <a href="javascript:void(0)" class="cart--btn cmn--btn-2 bg--primary purchaseBtn" data-id="{{ $product->id }}" data-name="{{ $product->name }}">@lang('Purchase Now')</a> --}}
                            @endif
                        @endif
                    </div>
                    <ul class="product-meta">
                        <li class="meta-item">
                            {{-- <div class="ratings"> --}}
                                <h5 class="review-count"><i class="fa fa-star rating-color"></i> 4.8/5 (100) | 10rb+ <span class="terjual">terjual</span></h5>
                            {{-- </div> --}}
                        </li>
                    </ul>
                    <ul class="product-meta">
                        <li class="meta-item">
                            <h6 class="title">@lang('Category') :</h6>
                            <a href="{{ route('products',$product->category_id) }}">{{ __($product->category->name) }}</a>
                        </li>
                        <li class="meta-item">
                            <h6 class="title">@lang('Tags') :</h6>
                            <div>
                                @foreach ($product->meta_keyword as $keyword)
                                    <a href="#0">{{ $keyword }}</a>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                    @if($product->specifications)
                    <div class="specifications mt-3">
                        <h5 class="title">@lang('Product Full Specifications')</h5>
                        <table class="specification-table">
                            <tbody>
                                @foreach($product->specifications as $specification)
                                <tr>
                                    <th>{{ $specification['name'] }}</th>
                                    <td>{{ $specification['value'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                    <div class="row mt-3">

                        @if (Auth::check())
                        <div class="col-5 col-md-3 col-lg-3">
                            <button onclick="CopyToClipboard()" class="btn btn--success btn-block btn-copy p-2" style="width: -webkit-fill-available;">Copy Link <i class="las la-clipboard"></i></button>
                        </div>

                        <div class="col-7 col-md-7 col-lg-7">
                            <button type="button" class="btn btn--info btn-copy px-3 py-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Share on social media
                                <i class="las la-share"></i>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape shape1"><img src="{{asset($activeTemplateTrue.'images/shape/blob1.png')}}" alt="shape"></div>
</section>

<div class="product-details pb-80 section-bg">
    <div class="container ">
        <div class="product-details-wrapper">
            <div class="description">
                <h3 class="mb-3">@lang('Description')</h3>
                @php echo @$product->description @endphp
            </div>
        </div>
    </div>
</div>





<!-- Products Section Starts Here -->
<section class="product-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <div class="section-header text-center">
                    <h2 class="title">@lang('Releted Products')</h2>
                </div>
            </div>
        </div>
        <div class="product-slider owl-carousel owl-theme owl-loaded owl-drag">
            @foreach($relateds as $prod)
            <div class="owl-item">
                <div class="product-item">
                    <div class="product-thumb">
                        <img src="{{getImage(imagePath()['products']['path'].'/'.$prod->thumbnail,imagePath()['products']['size'])}}" alt="products">
                        <ul class="product-options">
                            <li><a class="image" href="{{getImage(imagePath()['products']['path'].'/'.$prod->thumbnail,imagePath()['products']['size'])}}"><i class="las la-expand"></i></a></li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <h6 class="product-title"><a  href="{{ route('product.details',['id' => $prod->id, 'slug' => slug($prod->name)])}}" >{{ __(shortDescription($prod->name,35)) }}</a></h6>

                        @if ($prod->quantity>=0)
                        <span class="product-availablity text--success">@lang('in stock')</span>
                        @else
                        <span class="product-availablity text--danger">@lang('out stock')</span>

                        @endif


                        <div class="product-price">
                            <span class="current-price">{{ $general->cur_sym }}{{showAmount($prod->price)}}</span>
                        </div>
                        <a href="{{ route('product.details',['id' => $prod->id, 'slug' => slug($prod->name)])}}" class="add-to-cart cmn--btn-2">@lang('Details')</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="shape shape2"><img src="./assets/images/shape/blob1.png" alt=""></div>
</section>
<!-- Products Section Ends Here -->

<!-- Modal -->
<div class="modal fade" id="purchaseModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">@lang('Purchase Confirmation')</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('user.purchase') }}" method="post">
            @csrf
            <div class="modal-body">
                <input type="hidden" name="quantity">
                <input type="hidden" name="product_id">
                <h6>@lang('Are you sure to buy') "<span class="p_name"></span>"</h6>
                <div class="col-12 row">
                    <div class="col-12">
                        <label for="">QTY</label>
                        <input class="form-control quantity" type="number" name="qty" id="qty" min="1" placeholder="qty"
                        required>
                    </div>
                </div>
                <div class="col-12 row">
                    <div class="col-7">
                        <label for="">OTP</label>
                        <input class="form-control" type="number" name="otp" id="otp" min="1" placeholder="OTP" value="">
                        
                    </div>
                    <div class="col-5">
                        <label for=""></label>
                        <button class="form-control btn btn--primary" id="submitButtonId">Request OTP Code</button>
                        {{-- <form id="otp" action="{{route('user.otp.product')}}" method="post">
                            @csrf
                        </form> --}}
                    </div>
                    <div class="form-group col-12" id="alert" hidden>
                        <label for="">
                            <small id="message">
                            </small>
                        </label>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">@lang('Close')</button>
                <button type="submit" class="btn btn--success">@lang('Purchase')</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection



@push('style')

<style>
    .variants-wrapper {
        margin-left: 15px;
    }
    .btn-copy{
        border-radius: 20px;
    }

        .mt-100{

            margin-top: 100px;

        }

        .modal-title {
            font-weight: 900;
        }

        .modal-content{
             border-radius: 13px;
        }

        .modal-body {
            color: #3b3b3b;
        }

        .img-thumbnail {
            border-radius: 33px;
            width: 61px;
            height: 61px;
        }

        .fab:before {
            position: relative;
            top: 13px;
        }

        .smd {
            width: 200px;
            font-size: small;
            text-align: center;
        }

        .modal-footer {
            display: block;
        }

        .ur {
            border: none;
            background-color: #e6e2e2;
            border-bottom-left-radius: 4px;
            border-top-left-radius: 4px;
        }

        .cpy {
            border: none;
            background-color: #e6e2e2;
            border-bottom-right-radius: 4px;
            border-top-right-radius: 4px;
            cursor: pointer;
        }

        button.focus,
        button:focus {
            outline: 0;
            box-shadow: none !important;

        }
  
        .ur.focus,
        .ur:focus {
            outline: 0;
            box-shadow: none !important;
        }

        .message{
                font-size: 11px;
    color: #ee5535;
        }

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
@endpush

@push('modal')
    
@if (Auth::check())
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content col-12">
        <div class="modal-header">
            <h5 class="modal-title">Share</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="icon-container1 d-flex">
                <div class="smd">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode( route('product.details',['id' => $product->id, 'slug' => slug($product->name)])."?ref=".auth()->user()->username) }}" target="_blank">
                        <i class="img-thumbnail fab fa-facebook fa-2x"
                        style="color: #3b5998;background-color: #eceff5;"></i>
                        <p>Facebook</p>
                    </a>
                </div>
                <div class="smd">
                    <a href="https://api.whatsapp.com/send?text={{ urlencode('Check out this product: ' . route('product.details',['id' => $product->id, 'slug' => slug($product->name)])."?ref=".auth()->user()->username) }}" target="_blank">
                        <i class="img-thumbnail fab fa-whatsapp fa-2x"
                        style="color:  #25D366;background-color: #cef5dc;"></i>
                        <p>Whatsapp</p>
                    </a>
                </div>
                <div class="smd">
                    <a href="https://www.facebook.com/dialog/send?link={{ urlencode(route('product.details',['id' => $product->id, 'slug' => slug($product->name)])."?ref=".auth()->user()->username) }}" target="_blank">
                        <i class="img-thumbnail fab fa-facebook-messenger fa-2x"
                            style="color: #3b5998;background-color: #eceff5;"></i>
                        <p>Messenger</p>
                    </a>
                </div>
                <div class="smd">
                    <a href="https://t.me/share/url?url={{ urlencode(route('product.details',['id' => $product->id, 'slug' => slug($product->name)])."?ref=".auth()->user()->username) }}&text=Check%20out%20this%20product" target="_blank">
                    <i class="img-thumbnail fab fa-telegram fa-2x"
                        style="color:  #4c6ef5;background-color: aliceblue"></i>
                    <p>Telegram</p>
                    </a>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>
@endif
@endpush


@push('script')
<script>
    (function($) {
        "use strict";

        $('.purchaseBtn').on('click',function(){
            var counter = $('.cart-count').val();
            var modal = $('#purchaseModal');
            modal.find('input[name=quantity]').val(counter);
            modal.find('input[name=qty]').val(counter);
            modal.find('input[name=product_id]').val($(this).data('id'));
            modal.find('.p_name').text($(this).data('name'));
            modal.find('.quantity').text(counter);
            modal.modal('show');
        });
    })(jQuery);

</script>

<script>
            $("#submitButtonId").click(function() {

                // console.log($('meta[name="csrf-token"]').attr('content'));
                var url = "{{route('user.otp.product')}}"; // the script where you handle the form input.

                $.ajax({
                    type: "POST",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    success: function(data)
                    {
                        // alert(data.message); // show response from the php script.
                        $("#alert").removeAttr('hidden');
                        $("#submitButtonId").prop('disabled', true);
                        $('#message').html(data.message);
                    }
                    });

                return false; // avoid to execute the actual submit of the form.
        });
</script>
<script>
    $(".cartBtn").click(function() {
            var product_id = $(this).data('id');
            var qty =  $('.cart-count').val();

            $.ajax({
                url: '{{ route("user.addToCart") }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    product_id: product_id,
                    qty: qty,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    // alert(response.message);
                    iziToast.success({
                        title: 'Success',
                        message: response.message,
                        position: 'topRight'
                    });
                    setTimeout(function() {
                    location.reload();
                    }, 2000);
                }
            });
        });
</script>
<script>
    $(".cartform").click(function() {
            var product_id = $(this).data('id');
            var qty =  $('.cart-count').val();

            var form = document.getElementById("cartform");

            // Setel nilai elemen input tersembunyi sesuai kebutuhan
            form.querySelector("#qty").value = qty;
            form.querySelector("#product_id").value = product_id;

            // Kirim formulir
            form.submit();
    });
    
</script>
@if (Auth::check())

<script>
    
    function CopyToClipboard() {
    // Create an input
        var input = document.createElement('input');
        // Set it's value to the text to copy, the input type doesn't matter
        input.value = "{{ route('product.details',['id' => $product->id, 'slug' => slug($product->name)])}}?ref={{auth()->user()->username}}";
        // add it to the document
        document.body.appendChild(input);
        // call select(); on input which performs a user like selection  
        input.select();
        // execute the copy command, this is why we add the input to the document
        document.execCommand("copy");
        // remove the input from the document
        document.body.removeChild(input);
        alert('Link copied')
    }
</script>
<script>
    function myFunction() {

        var input = document.createElement('input');
        // Set it's value to the text to copy, the input type doesn't matter
        input.value = "{{ route('product.details',['id' => $product->id, 'slug' => slug($product->name)])}}?ref={{auth()->user()->username}}";
        // add it to the document
        document.body.appendChild(input);
        // call select(); on input which performs a user like selection  
        input.select();
        // execute the copy command, this is why we add the input to the document
        document.execCommand("copy");
        // remove the input from the document
        document.body.removeChild(input);
        $(".message").text("link copied");
    }
</script>
@endif

@endpush
