@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body p-0 text-center">
                {{-- <h5 class="card-title mt-2" style="font-weight: 600;">Gacha Master</h5> --}}
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Reward')</th>
                                <th scope="col">@lang('Type')</th>
                                <th scope="col">@lang('Harga Modal')</th>
                                <th scope="col">@lang('QTY')</th>
                                <th scope="col">@lang('Total')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($redeem as $key => $item)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($item->name) }}</td>
                                <td data-label="@lang('Type')" class="text-capitalize">{{ __($item->type) }}</td>

                                <td data-label="@lang('Harga Modal')">Rp. {{nb($item->modal) }}
                                </td>
                                <td data-label="@lang('QTY')">{{nb($item->qty) }}
                                </td>
                                <td data-label="@lang('Total')">Rp. {{nb($item->qty * $item->modal) }}
                                </td>

                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $item->id }}"
                                        data-name="{{ $item->name }}"
                                        data-modal="{{ $item->modal }}"
                                        data-type="{{ $item->type }}"
                                        data-qty="{{ $item->qty }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $redeem->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>

<div id="edit-gacha" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Redeem Percent')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.redeem.update')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input class="form-control id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="font-weight-bold"> @lang('Gacha Percent') </label>
                            <div class="input-group">
                                <input type="text" class="form-control reward_percent" placeholder="10000" name="reward_percent" required>
                                <div class="input-group-prepend"><span class="input-group-text">%
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- edit modal--}}
<div id="edit-detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Redeem Detail')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.redeem.update.detail')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Harga Modal') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control modal2" placeholder="10000" name="modal" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('QTY')</label>
                            <input type="number" class="form-control qty" name="qty" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Prize Image') <small>(recommended image ratio 9:16)</small></label>
                            <input class="form-control form-control-lg image" type="file" accept="image/*"  onchange="loadFile(event)" name="images">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="add-detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add Redeem Detail')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.redeem.add.detail')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control id" type="hidden" name="item_id" value="{{$redeemmaster->id}}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Harga Modal') </label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">{{$general->cur_sym}}
                                    </span></div>
                                <input type="text" class="form-control modal2" placeholder="10000" name="modal" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('QTY')</label>
                            <input type="number" class="form-control qty" name="qty" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold"> @lang('Prize Image') <small>(recommended image ratio 9:16)</small></label>
                            <input class="form-control form-control-lg image" type="file" accept="image/*"  onchange="loadFile(event)" name="images">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

{{-- @push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-product"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush --}}

@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-detail"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush
@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                var modal = $('#edit-detail');
                modal.find('.name').val($(this).data('name'));
                modal.find('.modal2').val($(this).data('modal'));
                modal.find('.qty').val($(this).data('qty'));

                modal.find('select[name=type]').val($(this).data('type'));
              
                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.edit-gacha').on('click', function () {
                var modal = $('#edit-gacha');
                modal.find('.reward_percent').val($(this).data('reward_percent'));

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-detail').on('click', function () {
                var modal = $('#add-detail');
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                var modal = $('#edit-product-stok');
                modal.find('.name').val($(this).data('name'));
                modal.find('.stok').val($(this).data('stok'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush