@extends('admin.layouts.app')

@section('panel')

<div class="row">

    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class="table-responsive--md  table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th>@lang('User')</th>
                                <th>@lang('Date')</th>
                                <th>@lang('Product')</th>
                                {{-- <th>@lang('Quantity')</th> --}}
                                {{-- <th>@lang('Price')</th> --}}
                                <th>@lang('Total')</th>
                                <th>@lang('Shipping Address')</th>
                                <th>@lang('Status')</th>
                                <th>@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($orders as $order)
                            <tr>
                                <td data-label="@lang('User')">
                                    <span class="font-weight-bold">{{$order->user->fullname}}</span>
                                    <br>
                                    <span class="small">
                                        <a href="{{ route('admin.users.detail', $order->user->id) }}"><span>@</span>{{
                                            $order->user->username }}</a>
                                    </span>
                                </td>
                                <td data-label="@lang('Date')">{{ showDateTime($order->created_at) }}</td>
                                <td data-label="@lang('Detail')"><a href="" class="badge btn-primary det"
                                        id="det{{$order->id}}" data-toggle="modal"
                                        data-target="#detailprod{{$order->id}}"><i class="las la-info"></i>Detail</a>
                                </td>
                                {{-- <td data-label="@lang('Product')">{{ __($order->product->name) }}</td> --}}
                                {{-- <td data-label="@lang('Quantity')">{{ __($order->quantity) }}</td> --}}
                                {{-- <td data-label="@lang('Price')">{{ showAmount($order->price) }} {{
                                    $general->cur_text }}</td> --}}
                                <td data-label="@lang('Total')">{{ nb($order->total) }} {{ $general->cur_text }}</td>
                                <td data-label="@lang('Shipping Address')">{{ Str::limit($order->alamat,40) }}<button
                                        class="btn--info btn-rounded  badge detailAlm" data-alamat="{{$order->alamat}}"
                                        data-pos="{{$order->kode_pos}}" data-nhp="{{$order->no_telp_penerima}}"
                                        data-np="{{$order->nama_penerima}}"><i class="fa fa-info"></i></button></td>
                                <td data-label="@lang('Status')">
                                    @if($order->status == 0)
                                    <span class="badge badge--warning">@lang('Pending')</span>
                                    @elseif($order->status == 1)
                                    <span class="badge badge--success">@lang('Shipped')</span>
                                    @if($order->no_resi != null)
                                    <button class="btn--info btn-rounded  badge detailBtn"
                                        data-no_resi="{{$order->no_resi}}"><i class="fa fa-info"></i></button>
                                    @endif
                                    @else
                                    <span class="badge badge--danger">@lang('Cancelled')</span>
                                    @endif

                                </td>
                                <td data-label="@lang('Action')">
                                    <button class="btn btn--primary btn-sm orderBtn"
                                        data-action="{{ route('admin.order.status',$order->id) }}" @if($order->status !=
                                        0) disabled @endif>@lang('Order Status')</button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $orders->links() }}
            </div>
        </div><!-- card end -->
    </div>
</div>

<div class="modal fade" id="statusModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Update Order Status')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="las la-times"></i>
                </button>
            </div>
            <form action="" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>@lang('Order Status')</label>
                        <select class="form-control" name="status" id="orderStatus">
                            <option value="0" selected hidden>@lang('Select Status')</option>
                            <option value="1">@lang('Shipped')</option>
                            <option value="2">@lang('Cancel')</option>
                        </select>
                    </div>
                    <div class="form-group" id="proGroup" style="display: none;">
                        <label>@lang('Shipping Provider')</label>
                        <input type="text" class="form-control" name="pro" id="pro" placeholder="JNE/JNT/LION">
                    </div>
                    <div class="form-group" id="trackingNumberGroup" style="display: none;">
                        <label>@lang('Number Resi')</label>
                        <input type="text" class="form-control" name="no_resi" id="trackingNumber"
                            placeholder="ASD12345">
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn--dark">@lang('Cancel')</button>
                    <button type="submit" class="btn btn--primary">@lang('Submit')</button>
                </div>
            </form>
        </div>
    </div>
</div>


@foreach ($orders as $item)
<div id="detailprod{{$item->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- @foreach ($ex->detail as $item)

                @endforeach --}}
                <table class="table table--light style--two">
                    <thead>
                        <tr>
                            {{-- <th scope="col">@lang('#')</th> --}}
                            <th scope="col">@lang('Product')</th>
                            <th scope="col" width="20%">@lang('Qty')</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($item->detail as $ex)
                        <tr>
                            {{-- <td data-label="@lang('#')">{{ $devcart->firstItem()+$loop->index }}</td> --}}
                            <td data-label="@lang('Product')">

                                <div class="row d-flex">
                                    <div class="book">
                                        <img src="{{ getImage('assets/images/products/'. $ex->product->thumbnail,  null, true)}}"
                                            class="book-img">
                                    </div>
                                    <div class="my-auto flex-column d-flex pad-left">
                                        <h6 class="mob-text text-left font-weight-bold ml-2">Product Name : {{
                                            $ex->product->name }} </h6>
                                    </div>
                                </div>

                            </td>
                            <td data-label="@lang('Qty')">
                                {{-- {{ $ex->qty }} --}}
                                <div class="input-group">

                                    <input type="text" readonly class="form-control input-number"
                                        value="{{ $ex->qty }} pcs" min="1" max="{{$ex->product->quantity}}">

                                </div>
                            </td>



                        </tr>
                        @empty
                        <tr>
                            <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endforeach
<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Nomor Resi : <span class="withdraw-detail"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>

<div id="detailAlm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Alamat : <span class="withdraw-detail"></span>. <br> Kode Pos : <span
                        class="withdraw-detail1"></span>
                    <br> Nama Penerima : <span class="withdraw-detail3"></span>
                    <br> No Telp Penerima : <span class="withdraw-detail2"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    (function($){
        "use strict";

        $('.orderBtn').click(function(){
            var modal = $('#statusModal');
            modal.find('form').attr('action',$(this).data('action'));
            modal.modal('show');
        });
    })(jQuery);
</script>
<script>
    $('.detailBtn').on('click', function () {
                var modal = $('#detailModal');
                var feedback = $(this).data('no_resi');
                modal.find('.withdraw-detail').html(feedback);
                modal.modal('show');
    });
</script>
<script>
    $('.detailAlm').on('click', function () {
            var modal = $('#detailAlm');
            var feedback = $(this).data('alamat');
            var pos = $(this).data('pos');
            var nhp = $(this).data('nhp');
            var np = $(this).data('np');
            modal.find('.withdraw-detail').html(feedback);
            modal.find('.withdraw-detail1').html(pos);
            modal.find('.withdraw-detail2').html(nhp);
            modal.find('.withdraw-detail3').html(np);
            modal.modal('show');
    });
</script>

<script>
    // Fungsi untuk menanggapi perubahan pada elemen select
  document.getElementById('orderStatus').addEventListener('change', function() {
    // Dapatkan nilai yang dipilih
    var selectedStatus = this.value;
    var trackingNumber = document.getElementById('trackingNumber');
    var pro = document.getElementById('pro');

    // Tentukan apakah harus menampilkan atau menyembunyikan input nomor resi
    if (selectedStatus === '1') {
      document.getElementById('trackingNumberGroup').style.display = 'block';
      trackingNumber.setAttribute('required', 'required');
      document.getElementById('proGroup').style.display = 'block';
      pro.setAttribute('required', 'required');
    } else {
      document.getElementById('trackingNumberGroup').style.display = 'none';
      trackingNumber.removeAttribute('required');
      document.getElementById('proGroup').style.display = 'none';
      pro.removeAttribute('required');
    }
  });
</script>
@endpush