@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body p-0 text-center">
                {{-- <h5 class="card-title mt-2" style="font-weight: 600;">Gacha Master</h5> --}}
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('QTY')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($userplot as $key => $item)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Username')">{{ cekuser($item->user_id) }}</td>
                                <td data-label="@lang('QTY')">{{$item->qty}}
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
        </div>
    </div>
</div>


{{-- edit modal--}}
<div id="add-user" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Gacha Detail')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.gacha.user.plot.add')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <input class="form-control id" type="hidden" name="gacha_id" value="{{$gacha->id}}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('User')</label>
                            <select class="form-control selectpicker" data-live-search="true" name="user_id" required>
                                <option value="">@lang('Select User')</option>
                                @foreach ($user as $u)
                                    <option value="{{ $u->id }}">{{ $u->username }} - {{ $u->email }} - {{ $u->no_bro }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('QTY')</label>
                            <input type="number" class="form-control name" name="qty" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Add')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- @push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-product"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush --}}

@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-user"><i class="fa fa-fw fa-plus"></i>@lang('Add
    User')</a>
@endpush
@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                var modal = $('#edit-detail');
                modal.find('.name').val($(this).data('name'));
                modal.find('.modal2').val($(this).data('modal'));
                modal.find('.qty').val($(this).data('qty'));

                modal.find('select[name=type]').val($(this).data('type'));
              
                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.edit-gacha').on('click', function () {
                var modal = $('#edit-gacha');
                modal.find('.reward_percent').val($(this).data('reward_percent'));

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-detail').on('click', function () {
                var modal = $('#add-detail');
                modal.modal('show');
            });
            $('.add-user').on('click', function () {
                var modal = $('#add-user');
                modal.modal('show');
            });
            $('.selectpicker').selectpicker();

        })(jQuery);
</script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                var modal = $('#edit-product-stok');
                modal.find('.name').val($(this).data('name'));
                modal.find('.stok').val($(this).data('stok'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush
@push('script-lib')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>
@endpush
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
@endpush