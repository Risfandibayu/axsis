@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body p-0 text-center">
                {{-- <h5 class="card-title mt-2" style="font-weight: 600;">Gacha Master</h5> --}}
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Gacha Percent')</th>
                                <th scope="col">@lang('Item')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($gacha as $key => $item)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($item->name) }}</td>
                                {{-- <td data-label="@lang('Type')">{{ $item->weight }} Gram</td> --}}
                                <td data-label="@lang('Gacha Percent')">{{$item->reward_percent }}%
                                </td>
                                <td data-label="@lang('Item')">
                                    <a href="{{route('admin.gacha.detail',[$item->id])}}" class="btn btn-sm btn-primary">Detail Item <i class="la la-info"></i></a>
                                </td>
                                <td data-label="@lang('Status')">
                                    @if($item->status == 1)
                                    <span
                                        class="text--small badge font-weight-normal badge--success">@lang('Active')</span>
                                    @else
                                    <span
                                        class="text--small badge font-weight-normal badge--danger">@lang('Inactive')</span>
                                    @endif
                                </td>
                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $item->id }}"
                                        data-name="{{ $item->name }}"
                                        data-reward_percent="{{ $item->reward_percent }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $gacha->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>


{{-- edit modal--}}
<div id="edit-gacha" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Gacha')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.gacha.update')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input class="form-control id" type="hidden" name="id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Name')</label>
                            <input type="text" class="form-control name" name="name"  placeholder="Product ABC" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-weight-bold"> @lang('Gacha Percent') </label>
                            <div class="input-group">
                                <input type="text" class="form-control reward_percent" placeholder="10000" name="reward_percent" required>
                                <div class="input-group-prepend"><span class="input-group-text">%
                                    </span></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Non Active')"
                                name="status" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
{{-- 
@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-product"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>
@endpush --}}

@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                console.log($(this).data('image'));
                var modal = $('#edit-gacha');
                modal.find('.name').val($(this).data('name'));
                modal.find('.reward_percent').val($(this).data('reward_percent'));

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-product').on('click', function () {
                var modal = $('#add-product');
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    "use strict";
        (function ($) {
            $('.edits').on('click', function () {
                console.log($(this).data('image'));
                var modal = $('#edit-product-stok');
                modal.find('.name').val($(this).data('name'));
                modal.find('.stok').val($(this).data('stok'));
                // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });
        })(jQuery);
</script>
<script>
    var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src)
            }
        };

</script>
@endpush