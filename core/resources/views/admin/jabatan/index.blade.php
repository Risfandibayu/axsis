@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('#')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Type')</th>
                                <th scope="col">@lang('SP Requirement')</th>
                                <th scope="col">@lang('Limit')</th>
                                <th scope="col">@lang('Amount')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($jabatansweek as $key => $jabatan)
                            <tr>
                                <td data-label="@lang('#')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($jabatan->name) }}</td>
                                <td data-label="@lang('Type')">{{ $jabatan->type }} </td>
                                <td data-label="@lang('SP Requirement')">{{ $jabatan->req }} </td>
                                <td data-label="@lang('Limit')">{{ $jabatan->limit }} Days</td>
                                <td data-label="@lang('Amount')">{{ nb($jabatan->amount) }}</td>
                                <td data-label="@lang('Status')">
                                    @if($jabatan->status == 1)
                                    <span
                                        class="text--small badge font-weight-normal badge--success">@lang('Active')</span>
                                    @else
                                    <span
                                        class="text--small badge font-weight-normal badge--danger">@lang('Inactive')</span>
                                    @endif
                                </td>

                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $jabatan->id }}" data-name="{{ $jabatan->name }}"
                                        data-status="{{ $jabatan->status }}" data-type="{{ $jabatan->type }}"
                                        data-req="{{ $jabatan->req }}" data-limit="{{ $jabatan->limit }}"
                                        data-amount="{{ getAmount($jabatan->amount) }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $jabatansweek->links('admin.partials.paginate') }}
            </div>
        </div>
        <div class="card mt-25">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('#')</th>
                                <th scope="col">@lang('Name')</th>
                                <th scope="col">@lang('Type')</th>
                                <th scope="col">@lang('SP Requirement')</th>
                                <th scope="col">@lang('Limit')</th>
                                <th scope="col">@lang('Amount')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($jabatansmonth as $key => $jabatan)
                            <tr>
                                <td data-label="@lang('#')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($jabatan->name) }}</td>
                                <td data-label="@lang('Type')">{{ $jabatan->type }} </td>
                                <td data-label="@lang('SP Requirement')">{{ $jabatan->req }} </td>
                                <td data-label="@lang('Limit')">{{ $jabatan->limit }} Month</td>
                                <td data-label="@lang('Amount')">{{ nb($jabatan->amount) }}</td>
                                <td data-label="@lang('Status')">
                                    @if($jabatan->status == 1)
                                    <span
                                        class="text--small badge font-weight-normal badge--success">@lang('Active')</span>
                                    @else
                                    <span
                                        class="text--small badge font-weight-normal badge--danger">@lang('Inactive')</span>
                                    @endif
                                </td>

                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $jabatan->id }}" data-name="{{ $jabatan->name }}"
                                        data-status="{{ $jabatan->status }}" data-type="{{ $jabatan->type }}"
                                        data-req="{{ $jabatan->req }}" data-limit="{{ $jabatan->limit }}"
                                        data-amount="{{ getAmount($jabatan->amount) }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $jabatansmonth->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>


{{-- edit modal--}}
<div id="edit-jabatan" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Edit Jabatan')</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
            <form method="post" action="{{route('admin.jabatan.update')}}">
                @csrf
                <div class="modal-body">

                    <input class="form-control jabatan_id" type="hidden" name="id">

                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Name') :</label>
                        <input class="form-control name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Type') </label>
                        <select name="type" id="typeedit" class="form-control" disabled>
                            <option value="" selected hidden>-- Select Type --</option>
                            <option value="Weekly">Weekly</option>
                            <option value="Monthly">Monthly</option>
                        </select>
                    </div>

                    <div id="weeklyFormEdit" style="display: none;">
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('SP Requirement') :</label>
                            <input type="text" class="form-control wsp" name="weekly_sp">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Amount') :</label>
                            <input type="text" class="form-control wam" name="weekly_amount">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Limit (days)') :</label>
                            <input type="number" class="form-control wlm" name="weekly_limit">
                        </div>
                    </div>

                    <!-- Formulir Uang Bulanan dan Jumlah (Monthly) -->
                    <div id="monthlyFormEdit" style="display: none;">
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('SP Requirement') :</label>
                            <input type="text" class="form-control msp" name="monthly_sp">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Amount') :</label>
                            <input type="text" class="form-control mam" name="monthly_amount">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Limit (month)') :</label>
                            <input type="number" class="form-control mlm" name="monthly_limit">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-block btn--primary">@lang('Update')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="add-jabatan" class="modal  fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Add New Jabatan')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('admin.jabatan.store')}}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Name') :</label>
                        <input type="text" class="form-control " name="name" required>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold"> @lang('Type') </label>
                        <select name="type" id="typeadd" class="form-control" required>
                            <option value="" selected hidden>-- Select Type --</option>
                            <option value="Weekly">Weekly</option>
                            <option value="Monthly">Monthly</option>
                        </select>
                    </div>

                    <div id="weeklyForm" style="display: none;">
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('SP Requirement') :</label>
                            <input type="text" class="form-control" name="weekly_sp">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Amount') :</label>
                            <input type="text" class="form-control" name="weekly_amount">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Limit (days)') :</label>
                            <input type="number" class="form-control" name="weekly_limit">
                        </div>
                    </div>

                    <!-- Formulir Uang Bulanan dan Jumlah (Monthly) -->
                    <div id="monthlyForm" style="display: none;">
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('SP Requirement') :</label>
                            <input type="text" class="form-control" name="monthly_sp">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Amount') :</label>
                            <input type="text" class="form-control" name="monthly_amount">
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold"> @lang('Limit (month)') :</label>
                            <input type="number" class="form-control" name="monthly_limit">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold">@lang('Status')</label>
                            <input type="checkbox" data-width="100%" data-onstyle="-success" data-offstyle="-danger"
                                data-toggle="toggle" data-on="@lang('Active')" data-off="@lang('Inactive')"
                                name="status" checked>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn-block btn btn--primary">@lang('Submit')</button>
                </div>
            </form>

        </div>
    </div>
</div>


@endsection



@push('breadcrumb-plugins')
<a href="javascript:void(0)" class="btn btn-sm btn--success add-jabatan"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>

@endpush

@push('script')
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                var modal = $('#edit-jabatan');
                var weeklyForm = document.getElementById('weeklyFormEdit');
                var monthlyForm = document.getElementById('monthlyFormEdit');
                modal.find('.name').val($(this).data('name'));
                modal.find('select[name=type]').val($(this).data('type'));
                
                if ($(this).data('type') === 'Weekly') {
                    weeklyForm.style.display = 'block';
                    modal.find('.wsp').val($(this).data('req'));
                    modal.find('.wam').val($(this).data('amount'));
                    modal.find('.wlm').val($(this).data('limit'));
                } else if ($(this).data('type') === 'Monthly') {
                    monthlyForm.style.display = 'block';
                    modal.find('.msp').val($(this).data('req'));
                    modal.find('.mam').val($(this).data('amount'));
                    modal.find('.mlm').val($(this).data('limit'));
                }

                if($(this).data('status')){
                    modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
                    modal.find('input[name="status"]').prop('checked',true);

                }else{
                    modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
                    modal.find('input[name="status"]').prop('checked',false);
                }

                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            $('.add-jabatan').on('click', function () {
                var modal = $('#add-jabatan');
                modal.modal('show');
            });


            
        })(jQuery);
</script>

<script>
    // Menangkap elemen-elemen yang diperlukan
        var typeSelect = document.getElementById('typeadd');
        var weeklyForm = document.getElementById('weeklyForm');
        var monthlyForm = document.getElementById('monthlyForm');

        // Menambahkan event listener untuk perubahan pada elemen select
        typeSelect.addEventListener('change', function () {
            // Mendapatkan nilai terpilih
            var selectedType = typeSelect.value;

            // Menyembunyikan semua formulir
            weeklyForm.style.display = 'none';
            monthlyForm.style.display = 'none';

            // Menampilkan formulir yang sesuai berdasarkan pilihan
            if (selectedType === 'Weekly') {
                weeklyForm.style.display = 'block';
            } else if (selectedType === 'Monthly') {
                monthlyForm.style.display = 'block';
            }
        });
</script>
@endpush